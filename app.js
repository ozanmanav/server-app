require('module-alias/register');
const http = require('http');
const jwt = require('jsonwebtoken');
const express = require('express');
const chalk = require('chalk');
const dotenv = require('dotenv');
const rateLimit = require('express-rate-limit');
const bugsnag = require('@bugsnag/js');
const bugsnagExpress = require('@bugsnag/plugin-express');
const bugsnagClient = bugsnag('7691d070a721085cdf084fdabfd6ad1a', { notifyReleaseStages: ['production', 'staging'] });
const voyager = require('graphql-voyager/middleware');
const { importSchema } = require('graphql-import');
const typeDefs = importSchema('./graphql/schema.graphql');
const compression = require('compression');
const bodyParser = require('body-parser');
const { getMailFromMessageId, getHistory } = require('./helpers/gmailOperations');
const { watchingMailHandler } = require('./helpers/gmailHandlers');
const { Candidate, GmailHistory } = require('./models');
const gcloudPubSub = require(`@google-cloud/pubsub`);
const apollo = require(`apollo-server`);
const apolloPubSub = new apollo.PubSub();
const cors = require('cors');
var fs = require('fs');
var morgan = require('morgan');
var path = require('path');
const logger = require('./services/CloudWatchLogService');
const { watchCandidateEmailsJob, refreshAccessTokensJob } = require('./services/SchedulerJobService');

const { handleAsyncExceptions } = require('./util');

const run = async () => {
  try {
    //   create a write stream (in append mode)
    var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' });

    logger.log('info', '[STARTUP] Server Starting...', { tags: 'startup, app' });
    const createApolloServer = require('./infrastructure/apolloServer');
    const connectDatabase = require('./infrastructure/database');
    const routes = require('./routes');
    const FirebaseAdminService = require('./services/FirebaseAdminService');

    bugsnagClient.use(bugsnagExpress);
    dotenv.config();
    FirebaseAdminService.init();

    await connectDatabase();

    const app = express();
    // setup the logger
    app.use(morgan('combined', { stream: accessLogStream }));
    app.get('/access.log', function(req, res) {
      res.sendFile(path.join(__dirname + '/access.log'));
    });
    app.use(
      cors({
        origin: '*',
      })
    );

    app.enable('trust proxy');
    app.use(bodyParser.json());
    app.use(compression());

    app.use(
      rateLimit({
        windowMs: 5 * 60 * 1000,
        max: 1000,
      })
    );

    app.use('/voyager', voyager.express({ endpointUrl: '/graphql' }));

    if (process.env === 'production') {
      var bugsnagMiddleware = bugsnagClient.getPlugin('express');
      app.use(bugsnagMiddleware.requestHandler);
      app.use(bugsnagMiddleware.errorHandler);
    }

    app.use(async (req, res, next) => {
      const token = req.headers['authorization'];

      if (token && token !== 'null') {
        try {
          req.activeCandidate = await jwt.verify(token, process.env.SECRET_KEY);
          req.activeRecruiter = await jwt.verify(token, process.env.SECRET_KEY);
        } catch (error) {
          console.log(error);
        }
      }

      next();
    });

    app.use('/api/v1', routes);

    app.post('/api/v1/chat/webhook/conversation-created', (req, res) => {
      console.log('called');
      try {
        apolloPubSub.publish('message', {
          message: req.body,
        });
        res.status(200).json('ok');
      } catch (e) {
        console.log(e);
        res.status(500).json(e);
      }
    });

    try {
      const googlePubSub = new gcloudPubSub.PubSub({
        keyFilename: './helpers/talentenvoy-mobile-mvp-690566182b7c.json',
      });

      // Gmail watching user for new mails and push to this channel.
      const subscription = googlePubSub.subscription(
        'projects/talentenvoy-mobile-mvp/subscriptions/talentEnvoySubscription'
      );
      subscription.on(`message`, async message => {
        console.info('message', message);
        await watchingMailHandler(googlePubSub, message);
      });

      const subscriptionNewMail = googlePubSub.subscription(
        'projects/talentenvoy-mobile-mvp/subscriptions/newEmailSubscription'
      );
      subscriptionNewMail.on(`message`, async message => {
        const decodedLastMail = await Buffer.from(message.data, 'base64').toString();
        console.info('new mail received');
      });
    } catch (error) {
      console.error('PUBSUB Error ', error);
    }

    const apolloServer = await createApolloServer(typeDefs, bugsnagClient, apolloPubSub);

    apolloServer.applyMiddleware({ app });

    const httpServer = http.createServer(app);

    apolloServer.installSubscriptionHandlers(httpServer);

    httpServer.listen({ port: process.env.PORT || 8000 }, () => {
      console.info(chalk.yellow(`🚀 Server ready at http://localhost:${process.env.PORT}${apolloServer.graphqlPath}`));
      console.info(
        chalk.yellow(`🚀 Subscriptions ready at ws://localhost:${process.env.PORT}${apolloServer.subscriptionsPath}`)
      );
      console.info(chalk.yellow(`🗂 Schema Voyager ready at http://localhost:${process.env.PORT}/voyager`));
    });
    logger.log('info', '[STARTUP] Server Started...', { tags: 'startup, app' });

    await refreshAccessTokensJob();
    await watchCandidateEmailsJob();
  } catch (error) {
    logger.log('error', '[STARTUP] ##### APP ERROR #####' + error.stack, { tags: 'startup, app' });
    console.error(chalk.red('##### APP ERROR #####', error.stack));
    bugsnagClient.notify(error);
  }
};

module.exports = { run, apolloPubSub };

if (require.main === module) {
  handleAsyncExceptions();
  run();
}
