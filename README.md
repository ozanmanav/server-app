# TalentEnvoy Mobile App Server 
Server build using Node js + Mongo DB + GraphQL for serving data for mobile app(IOS and android).

## Getting Started
These instructions will get you started.
```
npm install
```

 Create a .env file for settung up following.
```
DB_URI= mongodb://teuser:pass1234@ds137404.mlab.com:37404/talentenvoydb
SECRET_KEY = 7C8843C4F4DB222725F18A7B7B4B7
PORT = 4001
```

Now start you server
```
npm start
```

Also start reddis server:
```
redis-server
```
It will run on 6379 port.

visit [http://localhost:4001/graphql](http://localhost:4001/graphql) for live GraphQL playground.
