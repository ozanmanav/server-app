'use strict';
const { ApolloServer, PubSub } = require('apollo-server-express');
const jwt = require('jsonwebtoken');
const { AuthenticationError, makeExecutableSchema } = require('apollo-server');
const { addDirectiveResolveFunctionsToSchema } = require('graphql-directive');
const redis = require('../helpers/redis');
const logger = require('../services/CloudWatchLogService');
const Models = require('../models');
const JSON = require('circular-json');
const createApolloServer = async (typeDefs, bugsnagClient, pubsub) => {
  const resolvers = require('../graphql/resolvers/index');

  const schema = makeExecutableSchema({
    typeDefs,
    resolvers,
  });

  addDirectiveResolveFunctionsToSchema(schema, {
    requireAuthCandidate(resolve, directiveArgs, obj, context, info) {
      if (!context.activeCandidate)
        throw new Error(`You must be authenticated candidate to access "${info.fieldName}"`);
      return resolve();
    },
    requireAuthRecruiter(resolve, directiveArgs, obj, context, info) {
      if (!context.activeRecruiter)
        throw new Error(`You must be authenticated recruiter to access "${info.fieldName}"`);
      return resolve();
    },
  });

  const server = new ApolloServer({
    introspection: true,
    playground: true,
    tracing: true,
    subscriptions: {
      onConnect: (connectionParams, webSocket) => {
        if (connectionParams.subcriptionAuthorization) {
          try {
            return {
              subscribedCandidate: jwt.verify(connectionParams.subcriptionAuthorization, process.env.SECRET_KEY),
            };
          } catch (error) {
            console.log(error);
          }
        }
      },
    },
    schema,
    context: ({ req, connection }) => {
      if (req) {
        logger.log('info', '[APOLLO/GRAPHQL] Request: ' + JSON.stringify(req.body), { tags: 'apollo, graphql' });
      }

      return {
        ...Models,
        pubsub,
        bugsnagClient,
        redis,
        logger,
        subscribedCandidate: connection && connection.context && connection.context.subscribedCandidate,
        activeCandidate: req ? req.activeCandidate : null,
        activeRecruiter: req ? req.activeRecruiter : null,
      };
    },
    formatError: error => {
      logger.log('error', '[APOLLO/GRAPHQL] Error: ' + error, { tags: 'apollo, graphql' });

      console.log(error);
      return error;
    },
    formatResponse: (response, { context: { activeCandidate } }) => {
      logger.log('info', '[APOLLO/GRAPHQL] Response: ' + JSON.stringify(response), { tags: 'apollo, graphql' });
      //console.log(response);
      return response;
    },
  });

  return server;
};

module.exports = createApolloServer;
