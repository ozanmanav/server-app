'use strict';
const mongoose = require('mongoose');
const cachegoose = require('cachegoose');
const chalk = require('chalk');

const logger = require('../services/CloudWatchLogService');

const connectDatabase = async () => {
  logger.log('info', '[STARTUP] Connecting to DB...', { tags: 'startup,mongo' });
  await mongoose
    .connect(process.env.DB_URI, {
      useCreateIndex: true,
      useNewUrlParser: true,
    })
    .then(() => logger.log('info', '[STARTUP] Connected to DB...', { tags: 'startup,mongo' }))
    .catch(error => console.log(error));

  // cachegoose(mongoose, {
  //   engine: 'redis' /* If you don't specify the redis engine,      */,
  //   port: process.env.REDIS_PORT /* the query results will be cached in memory. */,
  //   host: process.env.REDIS_URL,
  // });
};

module.exports = connectDatabase;
