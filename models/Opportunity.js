const { Schema, model } = require('mongoose');

const opportunitySchema = new Schema({
  jobCard: {
    type: Schema.Types.ObjectId,
    ref: 'jobCards',
  },
  candidate: {
    type: Schema.Types.ObjectId,
    ref: 'candidates',
  },
  recruiter: {
    type: Schema.Types.ObjectId,
    ref: 'recruiters',
  },
  status: {
    type: String,
    enum: ['pending', 'interested', 'notInterested'],
  },
  createdAt: {
    type: Date,
  },
  updatedAt: {
    type: Date,
  },
  notInterestedStatus: {
    type: Object,
    default: {
      isCompanyBlocked: false,
      reasonText: ``,
    },
  },
  jdResumeMatchRatio: {
    type: Number,
    required: false,
  },
});

module.exports = model('opportunities', opportunitySchema);
