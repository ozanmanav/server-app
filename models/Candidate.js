const { Schema, model } = require('mongoose');

const moment = require('moment');
const bcrypt = require('bcrypt');
const Answer = require('./Answer');
const Question = require('./Question');
const Position = require('./Position');
const { sendNotificationByStatus } = require('../services/FirebaseAdminService');
const { WAITING_ADDITIONAL_QUESTIONS } = require('../constants/statusCodes');

const candidateSchema = new Schema(
  {
    googleId: {
      type: String,
      unique: false,
      required: false,
    },
    email: {
      type: String,
      unique: true,
      required: true,
    },
    talentEmail: {
      type: String,
      unique: false,
      required: false,
    },
    password: {
      type: String,
      required: false,
    },
    idToken: {
      type: String,
      required: false,
    },
    createdAt: {
      type: Date,
    },
    updatedAt: {
      type: Date,
    },
    accessToken: {
      type: String,
      unique: false,
      required: false,
    },
    refreshToken: {
      type: String,
      unique: false,
      required: false,
    },
    name: {
      type: String,
      unique: false,
      required: false,
    },
    photo: {
      type: String,
      unique: false,
      required: false,
    },
    provider: {
      type: String,
      unique: false,
      required: false,
    },
    status: {
      type: String,
      enum: ['0', '1', '1_1', '1_2', '1_3', '1_4', '2_0', '2', '2_1', '2_2', '3', '3_1', '3_2', '4', '5'],
      unique: false,
      default: '0',
    },
    isOnline: {
      type: Boolean,
      unique: false,
    },
    position: {
      type: Schema.Types.ObjectId,
      ref: 'position',
    },
    answers: [
      {
        type: Schema.Types.ObjectId,
        ref: 'answer',
      },
    ],
    fcmToken: {
      type: String,
      unique: false,
      required: false,
    },
    fcmTokenUpdatedAt: {
      type: Date,
    },
    availability: {
      type: Object,
      default: {
        Monday: {
          isActive: false,
          timeZone: {
            value: 'Pacific Standard Time',
            abbr: 'PDT',
            offset: -7,
            isdst: true,
            text: '(UTC-08:00) Pacific Time (US & Canada)',
          },
          intervals: [{ start: '09:15', end: '10:00' }],
        },
        Tuesday: {
          isActive: false,
          timeZone: {
            value: 'Pacific Standard Time',
            abbr: 'PDT',
            offset: -7,
            isdst: true,
            text: '(UTC-08:00) Pacific Time (US & Canada)',
          },
          intervals: [{ start: '09:15', end: '10:00' }],
        },
        Wednesday: {
          isActive: false,
          timeZone: {
            value: 'Pacific Standard Time',
            abbr: 'PDT',
            offset: -7,
            isdst: true,
            text: '(UTC-08:00) Pacific Time (US & Canada)',
          },
          intervals: [{ start: '09:15', end: '10:00' }],
        },
        Thursday: {
          isActive: false,
          timeZone: {
            value: 'Pacific Standard Time',
            abbr: 'PDT',
            offset: -7,
            isdst: true,
            text: '(UTC-08:00) Pacific Time (US & Canada)',
          },
          intervals: [{ start: '09:15', end: '10:00' }],
        },
        Friday: {
          isActive: false,
          timeZone: {
            value: 'Pacific Standard Time',
            abbr: 'PDT',
            offset: -7,
            isdst: true,
            text: '(UTC-08:00) Pacific Time (US & Canada)',
          },
          intervals: [{ start: '09:15', end: '10:00' }],
        },
        Saturday: {
          isActive: false,
          timeZone: {
            value: 'Pacific Standard Time',
            abbr: 'PDT',
            offset: -7,
            isdst: true,
            text: '(UTC-08:00) Pacific Time (US & Canada)',
          },
          intervals: [{ start: '09:15', end: '10:00' }],
        },
        Sunday: {
          isActive: false,
          timeZone: {
            value: 'Pacific Standard Time',
            abbr: 'PDT',
            offset: -7,
            isdst: true,
            text: '(UTC-08:00) Pacific Time (US & Canada)',
          },
          intervals: [{ start: '09:15', end: '10:00' }],
        },
      },
    },
    notAvailability: {
      type: Object,
      default: {
        timeZone: {
          value: 'Pacific Standard Time',
          abbr: 'PDT',
          offset: -7,
          isdst: true,
          text: '(UTC-08:00) Pacific Time (US & Canada)',
        },
        startDay: '2019 Jan Mon 28',
        startTime: '11:15',
        endDay: '2019 Feb Fri 01',
        endTime: '13:45',
      },
    },
    callLimitationPerWeek: {
      type: Object,
      default: {
        isActive: false,
        count: 0,
      },
    },
    stopServiceStatus: {
      type: Object,
      default: {
        isStopped: false,
        reasonText: `I don't want use`,
      },
    },
    batchDate: {
      type: Date,
      default: moment().add(7, 'days'),
    },
    interestedOpportunies: [
      {
        type: Schema.Types.ObjectId,
        ref: 'opportunies',
      },
    ],
    notInterestedOpportunies: [
      {
        type: Schema.Types.ObjectId,
        ref: 'opportunies',
      },
    ],
    emailSettings: {
      type: Object,
      default: {
        period: {
          type: String,
          enum: ['monthly', 'quarterly', 'yearly'],
        },
        topics: [],
      },
    },
    additionalQuestions: [
      {
        type: Schema.Types.ObjectId,
        ref: 'question',
      },
    ],
    candidateVoxNumber: {
      type: String,
      unique: true,
    },
    voxUserName: {
      type: String,
      unique: true,
    },
    onlyInterestedOpportunityCalls: {
      type: Boolean,
      default: false,
    },
    onlyScheduledCalls: {
      type: Boolean,
      default: false,
    },
    hideMyName: {
      type: Boolean,
      default: false,
    },
    criteriaInfos: {
      type: JSON,
      default: {},
    },
  },
  { timestamps: true }
);

candidateSchema.methods.addAnswer = async function(questionName, value, isAdditional = false) {
  const candidate = this;

  const question = await Question.findOne({ name: questionName });

  if (!question) {
    return;
  }

  if (isAdditional) {
    console.log('additional', question.name);
    question.isAnswered = true;
    await question.save();
  }

  // Find the answer if exist update, if dont exist create one.
  const newAnswer = await Answer.findOneAndUpdate(
    { candidate, question },
    { value: value },
    { upsert: true, new: true, setDefaultsOnInsert: true },
    function(error, result) {
      if (error) {
        return null;
      }

      return result.save();
    }
  )
    .populate('question')
    .populate('candidate');

  if (!newAnswer) console.log(`Answer cannot create`);

  if (this.answers.indexOf(newAnswer._id) > -1) {
    // console.log(`${this._id} already has this answer obj, updated.`);
  } else {
    this.answers.push(newAnswer);
  }

  await this.save();

  return newAnswer;
};

candidateSchema.methods.bulkAddAnswers = async function(values, isAdditional = false) {
  values = JSON.parse(values);
  const bulkArray = [];
  for (const questionName in values) {
    if (!values.hasOwnProperty(questionName)) continue;

    const answerValue = values[questionName];

    if (questionName === 'jobTitle') {
      const position = await Position.findOne({ title: { $regex: answerValue, $options: 'i' } }).exec();

      if (position) {
        // Position found. Assigning position to user
        this.position = position;
      }
    }

    await this.addAnswer(questionName, answerValue, isAdditional);
  }

  return await this.save();
};

candidateSchema.methods.updateFCMToken = async function(fcmToken) {
  this.fcmToken = fcmToken;
  this.fcmTokenUpdatedAt = new Date();

  return await this.save();
};

candidateSchema.methods.updateAvailability = async function(availability, callLimitationPerWeek) {
  this.availability = availability;

  this.callLimitationPerWeek = {
    isActive: callLimitationPerWeek.isActive,
    count: Math.trunc(callLimitationPerWeek.count),
  };

  return await this.save();
};

candidateSchema.methods.updateStopService = async function(isStopped, reasonText) {
  this.stopServiceStatus = {
    isStopped,
    reasonText,
  };

  return await this.save();
};

candidateSchema.methods.updateCandidateStatus = async function(status, pubsub) {
  this.status = status;

  const newCandidate = await this.save();

  if (newCandidate && pubsub) {
    pubsub.publish('CANDIDATE_STATUS_CHANGED', {
      statusListener: newCandidate,
    });
  }

  return newCandidate;
};

candidateSchema.methods.updateBatchDate = async function(batchDate) {
  this.batchDate = batchDate;

  return await this.save();
};

candidateSchema.methods.updateOnlineStatus = async function(isOnline, pubsub) {
  this.isOnline = isOnline;

  const newCandidate = await this.save();

  if (newCandidate && pubsub) {
    pubsub.publish('CANDIDATE_STATUS_CHANGED', {
      statusListener: newCandidate,
      statusListenerForAdmin: newCandidate,
    });
  }

  return newCandidate;
};

candidateSchema.methods.sendAdditionalQuestion = async function(title, name, pubsub) {
  // 1- Check this question name was exist.
  const question = await Question.findOne({ name }).exec();

  if (question) {
    throw new Error('This question identify name already created.');
  }

  // 2- Create Question for additional.
  const additionalQuestion = await new Question({
    title,
    name,
    category: 'additional',
    optionType: 'string',
    type: 'custom',
    options: [],
    isAnswered: false,
  }).save();

  if (!additionalQuestion) {
    throw new Error('Additional Question can not created.');
  }

  // 3- Add additional question to candidate collection.
  if (this.additionalQuestions.indexOf(additionalQuestion._id) > -1) {
    // Already Have this question
  } else {
    this.additionalQuestions.push(additionalQuestion);
  }

  // Change status to additional questions waiting from user.
  // We will get additional questions from api at mobile client.
  this.status = WAITING_ADDITIONAL_QUESTIONS;

  const newCandidate = await this.save();

  if (newCandidate && pubsub) {
    pubsub.publish('CANDIDATE_STATUS_CHANGED', {
      statusListener: newCandidate,
    });

    sendNotificationByStatus(newCandidate);
  }

  return newCandidate;
};

candidateSchema.methods.updateTokens = async function(tokens) {
  this.idToken = tokens.idToken;
  this.accessToken = tokens.accessToken;
  this.refreshToken = tokens.refreshToken;
  this.updatedAt = new Date();

  const newCandidate = await this.save();

  return newCandidate;
};

candidateSchema.pre('save', function(next) {
  return next();
});

module.exports = model('candidates', candidateSchema);
