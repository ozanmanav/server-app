const { Schema, model } = require('mongoose');

const activityLogSchema = new Schema({
  action: {
    type: String,
  },
  timeStamp: {
    type: Date,
  },
  candidate: {
    type: Schema.Types.ObjectId,
    ref: 'candidates',
  },
});

module.exports = model('activityLogSchema', activityLogSchema);
