const { Schema, model } = require('mongoose');

const candidateContactMappingSchema = new Schema({
  candidate_id: {
    type: Number,
    required: true,
  },
  recruiter_id: {
    type: Number,
    required: true,
  },
  google_contact_id: {
    type: Number,
    required: true,
  },
});

module.exports = model('candidateContactMapping', candidateContactMappingSchema);
