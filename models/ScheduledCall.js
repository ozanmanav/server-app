const { Schema, model } = require('mongoose');

const scheduledCallSchema = new Schema({
  candidate: {
    required: true,
    type: Schema.Types.ObjectId,
    ref: 'candidates',
  },
  recruiter: {
    required: true,
    type: Schema.Types.ObjectId,
    ref: 'recruiters',
  },
  jobCard: {
    required: true,
    type: Schema.Types.ObjectId,
    ref: 'jobCards',
  },
  status: {
    type: String,
    enum: [
      'candidatePending',
      'candidateAccepted',
      'candidateRejected',
      'recruiterPending',
      'recruiterAccepted',
      'recruiterRejected',
    ],
  },
  scheduledDate: {
    required: true,
    type: Date,
  },
  createdAt: {
    type: Date,
  },
  recruiterScheduleRequests: {
    type: Object,
  },
  candidateScheduleRequests: {
    type: Object,
  },
});

module.exports = model('scheduledCalls', scheduledCallSchema);
