const { Schema, model } = require('mongoose');

const positionSchema = new Schema({
  title: {
    type: String,
    required: true,
    unique: true,
  },
  questions: [
    {
      type: Schema.Types.ObjectId,
      ref: 'question',
      default: 'default',
    },
  ],
});

module.exports = model('position', positionSchema);
