const { Schema, model } = require('mongoose');

const questionSchema = new Schema({
  title: {
    type: String,
    required: true,
  },
  category: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  optionType: {
    type: String,
    enum: ['boolean', 'string', 'list'],
    required: true,
  },
  type: {
    type: String,
    enum: ['custom', 'generic'],
    required: true,
  },
  options: {
    type: Schema.Types.Mixed,
    required: true,
  },
  inputTitle: {
    type: String,
    required: false,
  },
  isAnswered: {
    type: Boolean,
    required: false,
  },
});

module.exports = model('question', questionSchema);
