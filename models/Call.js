const { Schema, model } = require('mongoose');

const callSchema = new Schema({
  candidate: {
    required: true,
    type: Schema.Types.ObjectId,
    ref: 'candidates',
  },
  recruiter: {
    required: true,
    type: Schema.Types.ObjectId,
    ref: 'recruiters',
  },
  jobCard: {
    required: true,
    type: Schema.Types.ObjectId,
    ref: 'jobCards',
  },
  scheduled: {
    type: Schema.Types.ObjectId,
    ref: 'scheduledCalls',
  },
  isScheduled: {
    type: Boolean,
    default: false,
  },
  caller: {
    type: String,
    required: true,
    enum: ['recruiter', 'candidate'],
  },
  status: {
    required: true,
    type: String,
  },
  duration: {
    required: true,
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
  },
  note: {
    required: false,
    type: String,
  },
  recordUrl: {
    required: false,
    type: String,
  },
});

module.exports = model('calls', callSchema);
