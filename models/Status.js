const { Schema, model } = require('mongoose');

const statusSchema = new Schema({
  statusId: {
    type: String,
    required: true,
    unique: true,
  },
  desc: {
    type: String,
    required: true,
  },
});

module.exports = model('status', statusSchema);
