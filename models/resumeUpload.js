const { Schema, model } = require('mongoose');

const ResumeUploadSchema = new Schema({
  filename: {
    type: String,
    required: true,
  },
  mimetype: {
    type: String,
    required: true,
  },
  path: {
    type: String,
    required: true,
  },
  url: {
    type: String,
    required: true,
  },
  candidate: {
    required: true,
    type: Schema.Types.ObjectId,
    ref: 'candidates',
  },
  createdAt: {
    required: true,
    type: Date,
    default: Date.now,
  },
  isActive: {
    required: true,
    type: Boolean,
  },
  ETag: {
    required: true,
    type: String,
  },
  Bucket: {
    required: true,
    type: String,
  },
  Key: {
    required: true,
    type: String,
  },
});

module.exports = model('ResumeUpload', ResumeUploadSchema);
