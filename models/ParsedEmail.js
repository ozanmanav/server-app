const { Schema, model } = require('mongoose');

const parsedEmailSchema = new Schema({
  candidate_id: {
    type: String,
    required: false,
  },
  messageId: {
    type: String,
    required: true,
    unique: true,
  },
  emails_in_body: {
    type: String,
    required: false,
  },
  job_skills: {
    type: String,
    required: false,
  },
  location: {
    type: String,
    required: false,
  },
  job_titles: {
    type: String,
    required: false,
  },
  organization: {
    type: String,
    required: false,
  },
  people: {
    type: String,
    required: false,
  },
  phone: {
    type: String,
    required: false,
  },
  recruiter_id: {
    type: String,
    required: false,
  },
  sender_domain: {
    type: String,
    required: false,
  },
  sender_email: {
    type: String,
    required: false,
  },
  sender_name: {
    type: String,
    required: false,
  },
  snippet: {
    type: String,
    required: false,
  },
  subject: {
    type: String,
    required: false,
  },
  timestamp: {
    type: String,
    required: false,
  },
  plain_body: {
    type: String,
    required: false,
  },
  predicted_label: {
    type: String,
    required: false,
  },
  jd_match_ratio: {
    type: String,
    required: false,
  },
});

module.exports = model('parsedEmail', parsedEmailSchema);
