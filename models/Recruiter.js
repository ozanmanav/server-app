const { Schema, model } = require('mongoose');
const moment = require('moment');
const bcrypt = require('bcrypt');

const recruiterSchema = new Schema({
  googleId: {
    type: String,
    unique: false,
    required: false,
  },
  email: {
    type: String,
    unique: true,
    required: true,
  },
  password: {
    type: String,
    required: false,
  },
  idToken: {
    type: String,
    required: false,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  accessToken: {
    type: String,
    unique: false,
    required: false,
  },
  refreshToken: {
    type: String,
    unique: false,
    required: false,
  },
  name: {
    type: String,
    unique: false,
    required: false,
  },
  photo: {
    type: String,
    unique: false,
    required: false,
  },
  provider: {
    type: String,
    unique: false,
    required: false,
  },
  status: {
    type: String,
    enum: ['0', '1', '1_1', '1_2', '1_3', '1_4', '2_0', '2', '2_1', '2_2', '3', '3_1', '3_2', '4', '5'],
    unique: false,
    default: '0',
  },
  isOnline: {
    type: Boolean,
    unique: false,
  },
  onlineStatus: {
    type: Boolean,
    unique: false,
  },
  fcmToken: {
    type: String,
    unique: false,
    required: false,
  },
  fcmTokenUpdatedAt: {
    type: Date,
  },
  businessHours: {
    type: Object,
    default: {
      Monday: {
        isActive: false,
        timeZone: {
          value: 'Pacific Standard Time',
          abbr: 'PDT',
          offset: -7,
          isdst: true,
          text: '(UTC-08:00) Pacific Time (US & Canada)',
        },
        intervals: [{ start: '09:15', end: '10:00' }],
      },
      Tuesday: {
        isActive: false,
        timeZone: {
          value: 'Pacific Standard Time',
          abbr: 'PDT',
          offset: -7,
          isdst: true,
          text: '(UTC-08:00) Pacific Time (US & Canada)',
        },
        intervals: [{ start: '09:15', end: '10:00' }],
      },
      Wednesday: {
        isActive: false,
        timeZone: {
          value: 'Pacific Standard Time',
          abbr: 'PDT',
          offset: -7,
          isdst: true,
          text: '(UTC-08:00) Pacific Time (US & Canada)',
        },
        intervals: [{ start: '09:15', end: '10:00' }],
      },
      Thursday: {
        isActive: false,
        timeZone: {
          value: 'Pacific Standard Time',
          abbr: 'PDT',
          offset: -7,
          isdst: true,
          text: '(UTC-08:00) Pacific Time (US & Canada)',
        },
        intervals: [{ start: '09:15', end: '10:00' }],
      },
      Friday: {
        isActive: false,
        timeZone: {
          value: 'Pacific Standard Time',
          abbr: 'PDT',
          offset: -7,
          isdst: true,
          text: '(UTC-08:00) Pacific Time (US & Canada)',
        },
        intervals: [{ start: '09:15', end: '10:00' }],
      },
      Saturday: {
        isActive: false,
        timeZone: {
          value: 'Pacific Standard Time',
          abbr: 'PDT',
          offset: -7,
          isdst: true,
          text: '(UTC-08:00) Pacific Time (US & Canada)',
        },
        intervals: [{ start: '09:15', end: '10:00' }],
      },
      Sunday: {
        isActive: false,
        timeZone: {
          value: 'Pacific Standard Time',
          abbr: 'PDT',
          offset: -7,
          isdst: true,
          text: '(UTC-08:00) Pacific Time (US & Canada)',
        },
        intervals: [{ start: '09:15', end: '10:00' }],
      },
    },
  },
  notAvailability: {
    type: Object,
    default: {
      timeZone: {
        value: 'Pacific Standard Time',
        abbr: 'PDT',
        offset: -7,
        isdst: true,
        text: '(UTC-08:00) Pacific Time (US & Canada)',
      },
      startDay: '2019 Jan Mon 28',
      startTime: '11:15',
      endDay: '2019 Feb Fri 01',
      endTime: '13:45',
    },
  },
  callLimitationPerWeek: {
    type: Object,
    default: {
      isActive: false,
      count: 0,
    },
  },
  stopServiceStatus: {
    type: Object,
    default: {
      isStopped: false,
      reasonText: `I don't want use`,
    },
  },
  recruiterNumbers: {
    type: Array,
    unique: false,
  },
  recruiterCompanyName: {
    type: String,
  },
  recruiterCompanyLocation: {
    type: String,
  },
});

recruiterSchema.methods.updateFCMToken = async function(fcmToken) {
  this.fcmToken = fcmToken;
  this.fcmTokenUpdatedAt = new Date();

  return await this.save();
};

module.exports = model('recruiters', recruiterSchema);
