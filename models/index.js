const Candidate = require('./Candidate');
const Question = require('./Question');
const ResumeUpload = require('./resumeUpload');
const ResumeLink = require('./resumeLink');
const Position = require('./Position');
const Status = require('./Status');
const Answer = require('./Answer');
const ParsedEmail = require('./ParsedEmail');
const Recruiter = require('./Recruiter');
const Call = require('./Call');
const JobCard = require('./JobCard');
const Opportunity = require('./Opportunity');
const ScheduledCall = require('./ScheduledCall');
const GmailHistory = require('./GmailHistory');
const CandidateEmails = require('./CandidateEmails');
const SmartReply = require('./SmartReply');
module.exports = {
  Candidate,
  Question,
  ResumeUpload,
  ResumeLink,
  Position,
  Status,
  Answer,
  ParsedEmail,
  Recruiter,
  Call,
  JobCard,
  Opportunity,
  ScheduledCall,
  GmailHistory,
  CandidateEmails,
  SmartReply,
};
