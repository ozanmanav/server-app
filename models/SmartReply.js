const { Schema, model } = require('mongoose');

const smartReplySchema = new Schema({
  opportunityId: {
    type: String,
  },
  threadId: {
    type: String,
  },
  createdAt: {
    type: Date,
  },
  smartReplyContent: {
    type: JSON,
    required: true,
  },
  isSent: {
    type: Boolean,
  },
  sentAt: {
    type: Date,
  },
});

module.exports = model('smartReply', smartReplySchema);
