const { Schema, model } = require('mongoose');

const ResumeLinkSchema = new Schema({
  url: {
    type: String,
    required: true,
  },
  candidate: {
    required: true,
    type: Schema.Types.ObjectId,
    ref: 'candidates',
  },
  createdAt: {
    required: true,
    type: Date,
    default: Date.now,
  },
  isActive: {
    required: true,
    type: Boolean,
  },
});

module.exports = model('ResumeLink', ResumeLinkSchema);
