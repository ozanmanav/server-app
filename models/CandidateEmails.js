const { Schema, model } = require('mongoose');

const candidateEmailsSchema = new Schema({
  candidateId: {
    type: String,
    required: true,
  },
  candidateEmail: {
    type: String,
    required: true,
  },
  talentEmail: {
    type: String,
    required: true,
  },
  emailPayload: {
    type: Object,
    required: true,
  },
  customLabelId: {
    type: String,
    required: true,
  },
  emailId: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
  },
});

module.exports = model('candidateEmails', candidateEmailsSchema);
