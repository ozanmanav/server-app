const { Schema, model } = require('mongoose');

const answerSchema = new Schema({
  candidate: {
    type: Schema.Types.ObjectId,
    ref: 'candidates',
  },
  question: {
    type: Schema.Types.ObjectId,
    ref: 'question',
  },
  value: {
    type: Object,
    required: true,
  },
});

module.exports = model('answer', answerSchema);
