const { Schema, model } = require('mongoose');

const gmailLabelsSchema = new Schema({
  candidateId: {
    type: String,
    required: true,
  },
  googleLabelId: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  messageListVisibility: {
    type: String,
    required: true,
  },
  labelListVisibility: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    required: true,
  },
});

module.exports = model('gmailLabels', gmailLabelsSchema);
