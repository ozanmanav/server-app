const { Schema, model } = require('mongoose');

const gmailHistorySchema = new Schema({
  candidateId: {
    type: String,
    required: true,
  },
  prevHistory: {
    type: String,
    required: false,
  },
  currHistory: {
    type: String,
    required: false,
  },
});

module.exports = model('gmailHistory', gmailHistorySchema);
