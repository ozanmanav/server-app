const { Schema, model } = require('mongoose');

const statusLogSchema = new Schema({
  oldStatusId: {
    type: String,
    required: true,
    unique: true,
  },
  statusId: {
    type: String,
    required: true,
    unique: true,
  },
  timeStamp: {
    type: Date,
  },
  candidate: {
    type: Schema.Types.ObjectId,
    ref: 'candidates',
  },
});

module.exports = model('statusLog', statusLogSchema);
