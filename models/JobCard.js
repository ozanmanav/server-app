const { Schema, model } = require('mongoose');

const jobCardSchema = new Schema({
  recruiter: {
    type: Schema.Types.ObjectId,
    ref: 'recruiters',
  },
  companyName: {
    type: String,
  },
  location: {
    type: String,
  },
  title: {
    type: String,
  },
  contractType: {
    type: String,
  },
  positionType: {
    type: String,
  },
  description: {
    type: [String],
  },
  createdAt: {
    type: Date,
  },
  refNo: {
    type: String,
  },
});

module.exports = model('jobCards', jobCardSchema);
