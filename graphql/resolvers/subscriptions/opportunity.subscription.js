const { withFilter } = require('apollo-server');

module.exports = {
  opportunityListener: {
    subscribe: withFilter(
      (parent, args, { pubsub }) => {
        return pubsub.asyncIterator('OPPORTUNITY_ADDED');
      },
      (payload, variables, { subscribedCandidate }) => {
        if (subscribedCandidate && subscribedCandidate.candidate) {
          const candidateId = subscribedCandidate.candidate._id;
          return candidateId ? String(payload.opportunityListener.candidate._id) === candidateId : true;
        } else {
          return false;
        }
      }
    ),
  },
};
