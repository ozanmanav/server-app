module.exports = {
  message: {
    subscribe: (parent, args, { pubsub }) => {
      return pubsub.asyncIterator('message');
    },
  },
};
