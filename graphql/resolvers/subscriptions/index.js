const candidate = require('./candidate.subscription');
const recruiter = require('./recruiter.subscription');
const call = require('./call.subscription');
const opportunity = require('./opportunity.subscription');
const scheduledCall = require('./scheduledCall.subscription');
const chat = require('./chat.subscription');

const Subscription = {
  ...candidate,
  ...recruiter,
  ...call,
  ...opportunity,
  ...scheduledCall,
  ...chat,
};
module.exports = Subscription;
