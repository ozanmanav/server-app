const { withFilter } = require('apollo-server');

module.exports = {
  candidateScheduledCallListener: {
    subscribe: withFilter(
      (parent, args, { pubsub }) => {
        return pubsub.asyncIterator('CANDIDATE_SCHEDULED_CALL_ADDED');
      },
      (payload, variables, { subscribedCandidate }) => {
        if (subscribedCandidate && subscribedCandidate.candidate) {
          const candidateId = subscribedCandidate.candidate._id;
          return candidateId ? String(payload.candidateScheduledCallListener.candidate._id) === candidateId : false;
        } else {
          return false;
        }
      }
    ),
  },
};
