const { withFilter } = require('apollo-server-express');

module.exports = {
  statusListener: {
    subscribe: withFilter(
      (parent, args, { pubsub }) => {
        return pubsub.asyncIterator('CANDIDATE_STATUS_CHANGED');
      },
      (payload, variables, { subscribedCandidate }) => {
        if (subscribedCandidate && subscribedCandidate.candidate) {
          const candidateId = subscribedCandidate.candidate._id;
          return candidateId ? String(payload.statusListener.id) === candidateId : true;
        } else {
          return false;
        }
      }
    ),
  },
  statusListenerForAdmin: {
    subscribe: withFilter(
      (parent, args, { pubsub }) => {
        return pubsub.asyncIterator('CANDIDATE_STATUS_CHANGED');
      },
      (payload, variables) => {
        return variables.candidateId ? String(payload.statusListenerForAdmin.id) === variables.candidateId : true;
      }
    ),
  },
};
