const { withFilter } = require('apollo-server');

module.exports = {
  candidateCallListener: {
    subscribe: withFilter(
      (parent, args, { pubsub }) => {
        return pubsub.asyncIterator('CANDIDATE_CALL_ADDED');
      },
      (payload, variables, { subscribedCandidate }) => {
        if (subscribedCandidate && subscribedCandidate.candidate) {
          const candidateId = subscribedCandidate.candidate._id;
          return candidateId ? String(payload.candidateCallListener.candidate._id) === candidateId : false;
        } else {
          return false;
        }
      }
    ),
  },
  recruiterCallListener: {
    subscribe: withFilter(
      (parent, args, { pubsub }) => {
        return pubsub.asyncIterator('RECRUITER_CALL_ADDED');
      },
      (payload, variables) => {
        return variables.recruiterId ? String(payload.call.recruiter.id) === variables.recruiterId : true;
      }
    ),
  },
};
