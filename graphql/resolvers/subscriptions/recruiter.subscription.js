const { withFilter } = require('apollo-server-express');

module.exports = {
  candidate: {
    subscribe: (parent, args, { pubsub }) => pubsub.asyncIterator('recruiter_query'),
  },
  recruiterStatusListener: {
    subscribe: withFilter(
      (parent, args, { pubsub }) => {
        return pubsub.asyncIterator('RECRUITER_STATUS_CHANGED');
      },
      (payload, variables) => {
        return variables.recruiterId ? String(payload.recruiterStatusListener.id) === variables.recruiterId : true;
      }
    ),
  },
};
