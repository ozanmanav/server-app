const answerQuery = {
  answer: async (parent, { candidateEmail, questionId, candidateTalentEmail }, { Answer, Candidate }) => {
    let candidate;
    if (candidateEmail) {
      candidate = await Candidate.findOne({ email: candidateEmail });
    } else if (candidateTalentEmail) {
      candidate = await Candidate.findOne({ talentEmail: candidateTalentEmail });
    }

    if (!candidate) {
      throw new Error('Candidate not exists.');
    }

    const answer = await Answer.findOne({ candidate, question: questionId })
      .populate('candidate')
      .populate('question');

    return answer;
  },
  answers: async (parent, args, { Answer }) => {
    return await Answer.find({})
      .populate('candidate')
      .populate('question');
  },
  candidateAnswers: async (parent, args, { Answer, activeCandidate }) => {
    return await Answer.find({ candidate: activeCandidate.candidate._id })
      .populate('candidate')
      .populate('question');
  },
  candidateAnswersByEmail: async (parent, { candidateEmail }, { Answer, Candidate }) => {
    const candidate = await Candidate.findOne({ email: candidateEmail });

    if (!candidate) {
      throw new Error('Candidate not exists.');
    }

    return await Answer.find({ candidate })
      .populate('candidate')
      .populate('question');
  },
};

module.exports = answerQuery;
