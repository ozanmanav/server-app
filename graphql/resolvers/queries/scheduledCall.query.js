const scheduledCallQuery = {
  candidateScheduledCalls: async (parent, { limit, offset }, { ScheduledCall, activeCandidate }) => {
    return await ScheduledCall.find({ candidate: activeCandidate.candidate._id })
      .skip(offset)
      .limit(limit)
      .sort({ createdAt: 'desc' })
      .populate('candidate')
      .populate('jobCard');
  },
  recruiterScheduledCalls: async (parent, { limit, offset }, { ScheduledCall, activeRecruiter }) => {
    return await ScheduledCall.find({ recruiter: activeRecruiter.recruiter._id })
      .skip(offset)
      .limit(limit)
      .sort({ createdAt: 'desc' })
      .populate('recruiter')
      .populate('jobCard');
  },
};

module.exports = scheduledCallQuery;
