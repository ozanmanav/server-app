const resumeUploadQuery = {
  candidateResumeUploads: async (parent, args, { ResumeUpload, activeCandidate }) => {
    return await ResumeUpload.find({ candidate: activeCandidate.candidate._id }).populate('candidate');
  },
  candidateResumeUploadsById: async (parent, args, { ResumeUpload }) => {
    return await ResumeUpload.find({ candidate: args.candidateId });
  },
};

module.exports = resumeUploadQuery;
