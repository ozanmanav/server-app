const jobCardQuery = {
  jobCards: async (parent, args, { JobCard }) => {
    return await JobCard.find({});
  },
  recruiterJobCards: async (parent, args, { JobCard, activeRecruiter }) => {
    console.log(activeRecruiter.recruiter._id);
    if (!activeRecruiter) {
      throw new Error('Recruiter session not found, please log in again.');
    }

    const recruiterJobCards = await JobCard.find({ recruiter: activeRecruiter.recruiter._id });

    return recruiterJobCards;
  },
  jobCardByNumber: async (parent, { phoneNumber }, { JobCard, Recruiter }) => {
    const recruiter = await Recruiter.findOne({ phoneNumber });

    if (!recruiter) {
      throw new Error('Recruiter does not exists.');
    }

    const jobCard = await JobCard.findOne({ recruiter: recruiter._id }).populate({
      path: 'recruiter',
      populate: [{ path: 'recruiters' }],
    });

    if (!jobCard) {
      throw new Error('JobCard does not exists.');
    }

    return jobCard;
  },
};

module.exports = jobCardQuery;
