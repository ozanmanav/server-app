const callQuery = {
  candidateCalls: async (parent, { limit, offset }, { Call, activeCandidate }) => {
    return await Call.find({ candidate: activeCandidate.candidate._id })
      .skip(offset)
      .limit(limit)
      .sort({ createdAt: 'desc' })
      .populate('candidate')
      .populate('jobCard');
  },
  candidateCallsById: async (parent, { candidateId }, { Call, activeCandidate }) => {
    return await Call.find({ candidate: candidateId })
      .sort({ createdAt: 'desc' })
      .populate('candidate')
      .populate('recruiter')
      .populate('jobCard');
  },
  recruiterCalls: async (parent, { limit, offset }, { Recruiter, activeRecruiter, Call }) => {
    if (!activeRecruiter) {
      throw new Error('Recruiter session not found, please log in again.');
    }

    const recruiterCalls = await Call.find({ recruiter: activeRecruiter.recruiter._id })
      .skip(offset)
      .limit(limit)
      .sort({ createdAt: 'desc' })
      .populate('candidate')
      .populate('jobCard');
    console.log(recruiterCalls);

    return recruiterCalls;
  },
};

module.exports = callQuery;
