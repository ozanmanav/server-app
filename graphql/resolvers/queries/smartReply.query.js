const smartReplyQuery = {
  smartReplyByThreadId: async (parent, { threadId }, { SmartReply }) => {
    const smartReply = await SmartReply.find({ 'smartReplyContent.threadId': threadId }).populate({
      path: 'opportunity',
    });

    if (!smartReply) {
      throw new Error('SmartReply does not exists.');
    }

    return smartReply;
  },
};

module.exports = smartReplyQuery;
