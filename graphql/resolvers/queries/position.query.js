const positionQuery = {
  position: async (parent, args, { Position }) => {
    return await Position.findById(args.id)
      .populate('position')
      .populate('questions');
  },
  positions: async (parent, args, { Position }) => {
    return await Position.find({})
      .populate('position')
      .populate('questions');
  },
};

module.exports = positionQuery;
