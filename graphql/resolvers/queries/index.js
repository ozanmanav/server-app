const candidate = require('./candidate.query');
const recruiter = require('./recruiter.query');
const question = require('./question.query');
const resumeUpload = require('./resumeUpload.query');
const resumeLink = require('./resumeLink.query');
const answer = require('./answer.query');
const position = require('./position.query');
const call = require('./call.query');
const jobCard = require('./jobCard.query');
const opportunity = require('./opportunity.query');
const scheduledCall = require('./scheduledCall.query');
const smartReply = require('./smartReply.query');

const Query = {
  ...candidate,
  ...recruiter,
  ...question,
  ...resumeUpload,
  ...resumeLink,
  ...answer,
  ...position,
  ...call,
  ...jobCard,
  ...opportunity,
  ...scheduledCall,
  ...smartReply,
};

module.exports = Query;
