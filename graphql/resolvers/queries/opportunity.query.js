const moment = require('moment');
const opportunityQuery = {
  opportunity: async (parent, { opportunityId }, { Opportunity, activeCandidate }) => {
    return await Opportunity.findById(opportunityId)
      .populate('jobCard')
      .sort({ createdAt: 'desc' })
      .populate('recruiter');
  },
  opportunities: async (parent, args, { Opportunity, activeCandidate }) => {
    return await Opportunity.find({ candidate: activeCandidate.candidate._id })
      .populate('jobCard')
      .sort({ createdAt: 'desc' })
      .populate('recruiter');
  },
  opportunityByNumber: async (
    parent,
    { candidateVoxNumber, recruiterNumber },
    { Opportunity, Recruiter, Candidate, Call }
  ) => {
    // tek bir numaradan bulma.
    const recruiter = await Recruiter.findOne({ recruiterNumbers: recruiterNumber });

    if (!recruiter) {
      console.log('Recruiter does not exists.');
    }

    const candidate = await Candidate.findOne({ candidateVoxNumber });

    if (!candidate) {
      throw new Error('Candidate does not exists.');
    }

    // ### Limit Condition Check ###
    let limitReached = false;
    const { callLimitationPerWeek } = candidate;

    if (callLimitationPerWeek.isActive) {
      // Find success calls for this week
      const thisWeekCalls = await Call.find({
        caller: 'recruiter',
        createdAt: {
          $gte: new Date(new Date() - 7 * 60 * 60 * 24 * 1000),
        },
      });

      if (thisWeekCalls.length >= callLimitationPerWeek.count) {
        limitReached = true;
      }
    }

    // ### Availability Times Check ###
    const { availability } = candidate;
    let isAvailable = false;
    Object.keys(availability).forEach(function(day) {
      try {
        const format = 'hh:mm';
        const utcOffset = availability[day].timeZone.text.split('C')[1].slice(0, 3);
        const currentDay = moment()
          .utcOffset(utcOffset)
          .format('dddd');
        const currentTime = moment()
          .utcOffset(utcOffset)
          .format(format);
        if (day === currentDay && availability[day].isActive) {
          availability[day].intervals.some(interval => {
            if (moment(currentTime, format).isBetween(moment(interval.start, format), moment(interval.end, format))) {
              isAvailable = true;
            } else {
              isAvailable = false;
            }
            return isAvailable;
          });
        }
      } catch (error) {
        console.log('Availability check error.', error);
      }
    });

    // Get only first matched.
    let opportunity = await Opportunity.findOne({
      candidate: candidate._id,
      recruiter: recruiter && recruiter._id,
    })
      .populate({
        path: 'jobCard',
      })
      .populate({
        path: 'candidate',
      })
      .populate({
        path: 'recruiter',
      });

    if (!opportunity) {
      opportunity = {};
      console.log('Opportunity does not exists.');
    }

    opportunity.candidate = candidate;
    opportunity.limitReached = limitReached;
    opportunity.isOnline = candidate.isOnline;
    opportunity.isAvailable = isAvailable;

    return opportunity;
  },
};

module.exports = opportunityQuery;
