const resumeLinkQuery = {
  candidateResumeLinks: async (parent, args, { ResumeLink, activeCandidate }) => {
    return await ResumeLink.find({ candidate: activeCandidate.candidate._id }).populate('candidate');
  },
};

module.exports = resumeLinkQuery;
