const questionQuery = {
  question: async (parent, { id }, { Question }) => {
    return await Question.findById(id);
  },
  questions: async (parent, { type }, { Question }) => {
    return await Question.find({});
  },
};

module.exports = questionQuery;
