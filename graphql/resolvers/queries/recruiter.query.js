const query = {
  recruiter: async (parent, args, { Recruiter, pubsub, activeRecruiter }) => {
    return await Recruiter.findById(args.id);
  },
  recruiters: async (parent, args, { Recruiter }) => {
    return await Recruiter.find(args).sort({ createdAt: 'desc' });
  },
  activeRecruiter: async (parent, args, { activeRecruiter, Recruiter }) => {
    if (!activeRecruiter) {
      throw new Error('Recruiter session not found, please log in again.');
    }
    return await Recruiter.findById(activeRecruiter.recruiter._id);
  },
};

module.exports = query;
