const query = {
  candidate: async (parent, args, { Candidate, pubsub, activeCandidate }) => {
    const candidate = await Candidate.findById(args.id)
      .populate({
        path: 'answers',
        populate: [{ path: 'question' }],
      })
      .populate({
        path: 'position',
        populate: [{ path: 'question' }],
      })
      .populate({
        path: 'additionalQuestions',
        populate: [{ path: 'question' }],
      });

    return candidate;
  },
  candidateByEmail: async (parent, args, { Candidate, pubsub, activeCandidate }) => {
    const candidate = await Candidate.findOne({ email: args.email })
      .populate({
        path: 'answers',
        populate: [{ path: 'question' }],
      })
      .populate({
        path: 'position',
        populate: [{ path: 'question' }],
      })
      .populate({
        path: 'additionalQuestions',
        populate: [{ path: 'question' }],
      });

    return candidate;
  },
  candidateByTalentEmail: async (parent, args, { Candidate, pubsub, activeCandidate }) => {
    const candidate = await Candidate.findOne({ talentEmail: args.talentEmail })
      .populate({
        path: 'answers',
        populate: [{ path: 'question' }],
      })
      .populate({
        path: 'position',
        populate: [{ path: 'question' }],
      })
      .populate({
        path: 'additionalQuestions',
        populate: [{ path: 'question' }],
      });

    return candidate;
  },
  candidates: async (parent, args, { Candidate }) => {
    return await Candidate.find(args)
      .sort({ createdAt: 'desc' })
      .populate({
        path: 'answers',
        populate: [{ path: 'question' }],
      })
      .populate({
        path: 'position',
        populate: [{ path: 'question' }],
      });
  },
  activeCandidate: async (parent, args, { activeCandidate, Candidate, redis }) => {
    // const cachedCandidate = await redis.get('activeCandidate_' + activeCandidate.candidate._id);
    // if (cachedCandidate) {
    //   return JSON.parse(cachedCandidate);
    // }
    if (!activeCandidate) {
      return null;
    }

    const candidate = await Candidate.findById(activeCandidate.candidate._id)
      .populate({
        path: 'answers',
        populate: [{ path: 'question' }],
      })
      .populate({
        path: 'additionalQuestions',
        populate: [{ path: 'question' }],
      })
      .populate({
        path: 'position',
        populate: [{ path: 'questions' }],
      });

    // if (candidate) {
    //   await redis.set('activeCandidate_' + candidate._id, JSON.stringify(candidate));
    // }

    return candidate;
  },
  status: async (parent, args, { Status }) => {
    return await Status.find({});
  },
};

module.exports = query;
