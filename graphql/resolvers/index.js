// query resolvers
const Query = require('./queries/index');

// mutation resolvers
const Mutation = require('./mutations/index');

// Subscription resolvers
const Subscription = require('./subscriptions/index');

module.exports = {
  Query,
  Mutation,
  Subscription,
};
