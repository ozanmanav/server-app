const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const token = require('@helpers/token');
const sendEmail = require('@helpers/sendEmail');

const { sendNotificationByStatus } = require('@services/FirebaseAdminService');

const { google } = require('googleapis');

module.exports = {
  createRecruiter: async (parent, { email, password, provider }, { Recruiter }) => {
    const recruiter = await Recruiter.findOne({ email });

    if (recruiter) {
      throw new Error('Recruiter already exists.');
    }

    const newRecruiter = await new Recruiter({
      name,
      email,
      password: bcrypt.hashSync(password, 10),
      provider: 'default',
    }).save();

    // Sending ID For immeditely update fcm token
    return { token: token.generateRecruiter(newRecruiter, '7d'), id: newRecruiter.id, status: newRecruiter.status };
  },
  signInRecruiter: async (parent, { email, password }, { Recruiter }) => {
    const recruiter = await Recruiter.findOne({ email });

    if (!recruiter) {
      throw new Error('Recruiter does not exists.');
    }

    const validPassword = bcrypt.compareSync(password, recruiter.password);

    if (!validPassword) {
      throw new Error('Invalid Password');
    }

    return { token: token.generateRecruiter(recruiter, '7d'), id: recruiter.id, status: recruiter.status };
  },
  signInWithGoogleRecruiter: async (
    parent,
    { email, idToken, accessToken, serverAuthCode, name, photo },
    { Recruiter }
  ) => {
    const recruiter = await Recruiter.findOne({ email });

    // if (!recruiter) {
    //   // Recruiter Doesnt Exist DB -> Create
    //   const newRecruiter = await authenticateAndWatch(idToken, serverAuthCode, email, name, photo);

    //   if (!newRecruiter) {
    //     throw new Error('Recruiter does not created.');
    //   }
    //   // Sending ID For immeditely update fcm token
    //   return {
    //     token: token.genegenerateRecruiterrate(newRecruiter, '7d'),
    //     id: newRecruiter.id,
    //     status: newRecruiter.status,
    //   };
    // }

    // // Recruiter Exist DB -> Login
    // // SECURITY PROBLEM ?
    // const oauth2Client = new google.auth.OAuth2(
    //   process.env.CLIENT_ID,
    //   process.env.CLIENT_SECRET,
    //   process.env.CALLBACK_URL
    // );

    // const ticket = await oauth2Client.verifyIdToken({
    //   idToken: idToken,
    //   audience: process.env.CLIENT_ID, // Specify the CLIENT_ID of the app that accesses the backend
    //   // Or, if multiple clients access the backend:
    //   //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
    // });

    // const payload = ticket.getPayload();

    // if (recruiter.email !== payload.email) {
    //   throw new Error('Recruiter does not auth with this idToken.');
    // } else {
    //   console.info('Recruiter matched with idToken...');
    // }

    return { token: token.generateRecruiter(recruiter, '7d'), id: recruiter.id, status: recruiter.status };
  },
  forgotPasswordRecruiter: async (parent, { email }, { Recruiter }) => {
    //Is the email valid
    const recruiter = await Recruiter.findOne({ email });

    if (!recruiter) {
      return {
        msg: "Recruiter Email doesn't exist in our database",
      };
    }

    //Is recruiter had login using Google sign in method.
    if (recruiter.provider === 'google') {
      return {
        msg: "You can login using 'Signin using Google' feature.",
      };
    }

    // TODO : change redirectLink
    const generatedToken = token.generateRecruiter(recruiter, '1h');

    const redirectLink = `https://www.${process.env.BASE_LINK}/resetpassword?email=${email}&token=${generatedToken}`;

    return await sendEmail(recruiter, redirectLink).then(
      () => {
        return {
          msg: 'Password Reset link send',
        };
      },
      error => {
        return {
          msg: 'Error sending email',
        };
      }
    );
  },

  resetPasswordRecruiter: async (parent, { email, password, token }, { Recruiter }) => {
    const recruiter = await Recruiter.findOne({ email });

    if (!recruiter) {
      throw new Error('Recruiter does not exists.');
    }

    const payload = await jwt.verify(token, process.env.SECRET_KEY);

    if (payload.email !== recruiter.email) {
      throw new Error('Invalid token');
    }

    const updatedRecruiter = await Recruiter.findByIdAndUpdate(recruiter._id, {
      $set: {
        password: bcrypt.hashSync(password, 10),
      },
    });

    if (updatedRecruiter) {
      return {
        msg: 'Password Resetted',
      };
    } else {
      return {
        msg: 'Password Not Resetted',
      };
    }
  },
  updateRecruiterFCMToken: async (parent, { recruiterId, fcmToken }, { Recruiter }) => {
    const recruiter = await Recruiter.findOne({ _id: recruiterId });

    if (!recruiter) {
      throw new Error('Recruiter does not exists.');
    }

    return await recruiter.updateFCMToken(fcmToken);
  },
  updateRecruiterBusinessHours: async (
    parent,
    { businessHours, callLimitationPerWeek },
    { Recruiter, activeRecruiter }
  ) => {
    if (!activeRecruiter) {
      throw new Error('Recruiter session not found, please log in again.');
    }

    const recruiter = await Recruiter.findOne({ _id: activeRecruiter.recruiter._id });

    if (!recruiter) {
      throw new Error('Recruiter does not exists.');
    }

    return await recruiter.updateBusinessHours(businessHours, callLimitationPerWeek);
  },
  updateRecruiterStopService: async (parent, { isStopped, reasonText }, { Recruiter, activeRecruiter }) => {
    if (!activeRecruiter) {
      throw new Error('Recruiter session not found, please log in again.');
    }

    const recruiter = await Recruiter.findOne({ _id: activeRecruiter.recruiter._id });

    if (!recruiter) {
      throw new Error('Recruiter does not exists.');
    }

    return await recruiter.updateStopService(isStopped, reasonText);
  },
  updateRecruiterStatus: async (parent, { recruiterId, status, notifyRecruiter = true }, { Recruiter, pubsub }) => {
    const recruiter = await Recruiter.findOne({ _id: recruiterId });

    if (!recruiter) {
      throw new Error('Recruiter does not exists.');
    }

    const newRecruiter = await recruiter.updateRecruiterStatus(status, pubsub);

    if (!newRecruiter) {
      throw new Error('Error on update recruiter status');
    }

    if (notifyRecruiter) {
      sendNotificationByStatus(newRecruiter);
    }

    return newRecruiter;
  },
  updateRecruiterBatchDate: async (parent, { batchDate }, { Recruiter, activeRecruiter }) => {
    if (!activeRecruiter) {
      throw new Error('Recruiter session not found, please log in again.');
    }

    const recruiter = await Recruiter.findOne({ _id: activeRecruiter.recruiter._id });

    if (!recruiter) {
      throw new Error('Recruiter does not exists.');
    }

    return await recruiter.updateBatchDate(batchDate);
  },
  updateRecruiterOnlineStatus: async (parent, { isOnline, recruiterId }, { Recruiter, activeRecruiter, pubsub }) => {
    const recruiter = await Recruiter.findOne({ _id: recruiterId });

    if (!recruiter) {
      throw new Error('Recruiter does not exists.');
    }

    return await recruiter.updateOnlineStatus(isOnline, pubsub);
  },
};
