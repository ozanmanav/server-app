const { WAITING_APPROVE } = require('@constants/statusCodes');

module.exports = {
  addAnswer: async (parent, { questionName, value }, { Candidate, pubsub, activeCandidate }) => {
    const candidate = await Candidate.findOne({ _id: activeCandidate.candidate._id });

    if (!candidate) {
      throw new Error('Candidate does not exists.');
    }

    const newCandidate = await candidate.addAnswer(questionName, value);

    return newCandidate;
  },
  bulkAddAnswersOnboardingWelcome: async (parent, { values }, { Candidate, activeCandidate }) => {
    const candidate = await Candidate.findOne({ _id: activeCandidate.candidate._id });

    if (!candidate) {
      throw new Error('Candidate does not exists.');
    }

    const newCandidate = await candidate.bulkAddAnswers(values);

    return newCandidate;
  },
  bulkAddAnswersAdditionalQuestions: async (parent, { values }, { Candidate, activeCandidate }) => {
    const candidate = await Candidate.findOne({ _id: activeCandidate.candidate._id });

    if (!candidate) {
      throw new Error('Candidate does not exists.');
    }

    const isAdditional = true;
    const newCandidate = await candidate.bulkAddAnswers(values, isAdditional);

    return newCandidate;
  },
};
