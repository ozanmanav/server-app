module.exports = {
  addCall: async (
    parent,
    { candidateId, recruiterId, scheduledId, jobCardId, caller, status, duration, isScheduled, recordUrl },
    { Candidate, Recruiter, ScheduledCall, Call, JobCard, pubsub }
  ) => {
    const candidate = await Candidate.findOne({ _id: candidateId });

    if (!candidate) {
      throw new Error('Candidate does not exists.');
    }

    const recruiter = await Recruiter.findOne({ _id: recruiterId });

    if (!recruiter) {
      throw new Error('Recruiter does not exists.');
    }

    const jobCard = await JobCard.findOne({ _id: jobCardId });

    if (!jobCard) {
      throw new Error('Job Card does not exists.');
    }

    if (isScheduled && scheduledId) {
      const scheduledCall = await ScheduledCall.findOne({ _id: scheduledId });

      if (!scheduledCall) {
        throw new Error('Scheduled Call does not exists.');
      }
    }

    const newCall = await new Call({
      candidate: candidate.id,
      recruiter: recruiter.id,
      jobCard: jobCardId,
      scheduled: scheduledId,
      isScheduled,
      caller,
      status,
      duration,
      createdAt: new Date(),
      recordUrl,
    })
      .save()
      .then(c => c.populate('jobCard').execPopulate())
      .then(c => c.populate('candidate').execPopulate());

    if (caller === 'recruiter') {
      pubsub.publish('RECRUITER_CALL_ADDED', {
        candidateCallListener: newCall,
      });
    } else if (caller === 'candidate') {
      pubsub.publish('CANDIDATE_CALL_ADDED', {
        candidateCallListener: newCall,
      });
    }

    return newCall;
  },
};
