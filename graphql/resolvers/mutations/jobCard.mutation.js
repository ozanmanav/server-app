module.exports = {
  addJobCard: async (
    parent,
    { recruiterId, companyName, location, title, contractType, positionType, description, refNo },
    { JobCard }
  ) => {
    const newJobCard = await new JobCard({
      recruiter: recruiterId,
      companyName,
      location,
      title,
      contractType,
      positionType,
      description,
      refNo,
    }).save();

    return newJobCard;
  },
};
