const shortid = require('shortid');
const mkdirp = require('mkdirp');
const moment = require('moment');

module.exports = {
  createResumeLink: async (parent, { url }, { Candidate, ResumeLink, pubsub, activeCandidate }) => {
    const candidate = await Candidate.findOne({ _id: activeCandidate.candidate._id });

    if (!candidate) {
      throw new Error('Candidate does not exists.');
    }

    if (!url) {
      throw new Error('Resume link is required');
    }

    const resumelink = await ResumeLink.findOneAndUpdate(
      { candidate: activeCandidate.candidate._id },
      {
        $set: {
          url,
        },
      }
    );

    if (resumelink) {
      console.log(`resumeLinkUpdated`);
    }

    const newResumeLink = await new ResumeLink({
      url,
      candidate,
      isActive: true,
    }).save();

    return newResumeLink;
  },
  deleteResumeLink: async (parent, { data: { url } }, { Candidate, ResumeLink, activeCandidate }) => {
    const candidate = await Candidate.findOne({ _id: activeCandidate.candidate._id });

    if (!candidate) {
      throw new Error('Candidate does not exists.');
    }

    if (!url) {
      throw new Error('Resume url is required');
    }

    const resumelink = await ResumeLink.findOne({ candidate: activeCandidate.candidate._id, filename });

    if (!resumelink) {
      throw new Error('Resume not found');
    }

    return await ResumeLink.remove();
  },
};
