module.exports = {
  addScheduledCall: async (
    parent,
    { candidateId, recruiterId, jobCardId, status, scheduledDate },
    { Candidate, Recruiter, ScheduledCall, JobCard, pubsub }
  ) => {
    const candidate = await Candidate.findOne({ _id: candidateId });

    if (!candidate) {
      throw new Error('Candidate does not exists.');
    }

    const recruiter = await Recruiter.findOne({ _id: recruiterId });

    if (!recruiter) {
      throw new Error('Recruiter does not exists.');
    }

    const jobCard = await JobCard.findOne({ _id: jobCardId });

    if (!jobCard) {
      throw new Error('Job Card does not exists.');
    }

    const newScheduledCall = await new ScheduledCall({
      candidate: candidate.id,
      recruiter: recruiter.id,
      jobCard: jobCardId,
      status,
      scheduledDate,
      createdAt: new Date(),
    })
      .save()
      .then(c => c.populate('jobCard').execPopulate())
      .then(c => c.populate('candidate').execPopulate());

    pubsub.publish('CANDIDATE_SCHEDULED_CALL_ADDED', {
      candidateScheduledCallListener: newScheduledCall,
    });

    return newScheduledCall;
  },
  updateScheduledCall: async (parent, args, { Candidate, Recruiter, ScheduledCall, JobCard, pubsub }) => {
    const updateScheduledCall = await ScheduledCall.findByIdAndUpdate(args.scheduledCallId, {
      $set: args,
    });

    return updateScheduledCall;
  },
};
