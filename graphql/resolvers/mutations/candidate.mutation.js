const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const token = require('@helpers/token');
const sendEmail = require('@helpers/sendEmail');
const { watchEmail } = require('@helpers/gmailOperations');
const { sendNotificationByStatus } = require('@services/FirebaseAdminService');

const { google } = require('googleapis');

module.exports = {
  createCandidate: async (parent, { email, password, provider, name }, { Candidate }) => {
    const candidate = await Candidate.findOne({ email });

    if (candidate) {
      throw new Error('Candidate already exists.');
    }

    const newCandidate = await new Candidate({
      name,
      email,
      password: bcrypt.hashSync(password, 10),
      provider: 'default',
    }).save();

    newCandidate.token = token.generate(newCandidate, '7d');
    return newCandidate;
  },
  signInCandidate: async (parent, { email, password }, { Candidate }) => {
    console.log(email);
    const candidate = await Candidate.findOne({ email });

    if (!candidate) {
      throw new Error('Candidate does not exists.');
    }

    const validPassword = bcrypt.compareSync(password, candidate.password);

    if (!validPassword) {
      throw new Error('Invalid Password');
    }

    candidate.token = token.generate(candidate, '7d');

    return candidate;
  },
  signInWithGoogle: async (parent, { serverAuthCode, email, name, password }, { Candidate }) => {
    const oAuth2Client = new google.auth.OAuth2(
      process.env.CLIENT_ID,
      process.env.CLIENT_SECRET,
      process.env.CALLBACK_URL
    );

    const tokenData = await oAuth2Client.getToken(serverAuthCode);

    console.log(tokenData);
    oAuth2Client.setCredentials(tokenData.tokens);

    const ticket = await oAuth2Client.verifyIdToken({
      idToken: tokenData.tokens.id_token,
      audience: process.env.CLIENT_ID,
    });

    var gmail = google.gmail({
      version: 'v1',
      auth: oAuth2Client,
    });

    const payload = ticket.getPayload();

    const candidate = await Candidate.findOne({ talentEmail: payload.email });

    if (!candidate) {
      const newCandidate = await new Candidate({
        name,
        email,
        password: bcrypt.hashSync(password, 10),
        idToken: tokenData.tokens.id_token,
        provider: 'google',
        accessToken: tokenData.tokens.access_token,
        refreshToken: tokenData.tokens.refresh_token || null,
        photo: payload.picture,
        talentEmail: payload.email,
      }).save();

      if (!newCandidate) {
        throw new Error('Candidate does not created.');
      }

      await watchEmail(gmail, newCandidate);

      // Sending ID For immeditely update fcm token
      newCandidate.token = token.generate(newCandidate, '7d');

      return newCandidate;
    }

    await watchEmail(gmail, candidate);

    candidate.name = name;
    candidate.password = bcrypt.hashSync(password, 10);
    candidate.email = email;
    candidate.accessToken = tokenData.tokens.access_token;
    candidate.idToken = tokenData.tokens.id_token;
    candidate.refreshToken = tokenData.tokens.refresh_token;

    await candidate.save();

    if (candidate.talentEmail !== payload.email) {
      throw new Error('Candidate does not auth with this idToken.');
    } else {
      console.info('Candidate matched with idToken...');
    }

    candidate.token = token.generate(candidate, '7d');

    return candidate;
  },
  forgotPassword: async (parent, { email }, { Candidate }) => {
    //Is the email valid
    const candidate = await Candidate.findOne({ email });
    if (!candidate) {
      return {
        msg: "Email doesn't exist in our database",
      };
    }

    //Is candidate had login using Google sign in method.
    if (candidate.provider === 'google') {
      return {
        msg: "You can login using 'Signin using Google' feature.",
      };
    }

    // TODO : change redirectLink
    const generatedToken = token.generate(candidate, '1h');

    const redirectLink = `https://www.${process.env.BASE_LINK}/resetpassword?email=${email}&token=${generatedToken}`;

    return await sendEmail(candidate, redirectLink).then(
      () => {
        console.log('Password Reset link send');
        return {
          msg: 'Password Reset link send',
        };
      },
      error => {
        console.log(error);
        return {
          msg: 'Error sending email',
        };
      }
    );
  },

  resetPassword: async (parent, { email, password, token }, { Candidate }) => {
    const candidate = await Candidate.findOne({ email });

    if (!candidate) {
      throw new Error('Candidate does not exists.');
    }

    const payload = await jwt.verify(token, process.env.SECRET_KEY);

    if (payload.candidate.email !== candidate.email) {
      throw new Error('Invalid token');
    }

    const updatedCandidate = await Candidate.findByIdAndUpdate(candidate._id, {
      $set: {
        password: bcrypt.hashSync(password, 10),
      },
    });

    if (updatedCandidate) {
      return {
        msg: 'Password Resetted',
      };
    } else {
      return {
        msg: 'Password Not Resetted',
      };
    }
  },
  assignPositionToCandidate: async (parent, { email, positionId }, { Candidate, Position }) => {
    const candidate = await Candidate.findOne({ email });

    if (!candidate) {
      throw new Error('Candidate does not exists.');
    }

    const position = await Position.findOne({ _id: positionId });

    if (!position) {
      throw new Error('Position does not exists.');
    }

    return Candidate.findOneAndUpdate(email, {
      $set: {
        position,
      },
    }).populate('position');
  },
  updateCandidateFCMToken: async (parent, { candidateId, fcmToken }, { Candidate }) => {
    const candidate = await Candidate.findOne({ _id: candidateId });

    if (!candidate) {
      throw new Error('Candidate does not exists.');
    }

    return await candidate.updateFCMToken(fcmToken);
  },
  updateCandidateAvailability: async (
    parent,
    { availability, callLimitationPerWeek },
    { Candidate, activeCandidate }
  ) => {
    const candidate = await Candidate.findOne({ _id: activeCandidate.candidate._id });

    if (!candidate) {
      throw new Error('Candidate does not exists.');
    }

    return await candidate.updateAvailability(availability, callLimitationPerWeek);
  },
  updateStopService: async (parent, { isStopped, reasonText }, { Candidate, activeCandidate }) => {
    const candidate = await Candidate.findOne({ _id: activeCandidate.candidate._id });

    if (!candidate) {
      throw new Error('Candidate does not exists.');
    }

    return await candidate.updateStopService(isStopped, reasonText);
  },
  updateCandidateStatus: async (parent, { candidateId, status, notifyCandidate = true }, { Candidate, pubsub }) => {
    const candidate = await Candidate.findOne({ _id: candidateId });

    if (!candidate) {
      throw new Error('Candidate does not exists.');
    }

    const newCandidate = await candidate.updateCandidateStatus(status, pubsub);

    if (!newCandidate) {
      throw new Error('Error on update candidate status');
    }

    if (notifyCandidate) {
      sendNotificationByStatus(newCandidate);
    }

    return newCandidate;
  },
  updateBatchDate: async (parent, { batchDate }, { Candidate, activeCandidate }) => {
    const candidate = await Candidate.findOne({ _id: activeCandidate.candidate._id });

    if (!candidate) {
      throw new Error('Candidate does not exists.');
    }

    return await candidate.updateBatchDate(batchDate);
  },
  updateOnlineStatus: async (parent, { isOnline, candidateId }, { Candidate, activeCandidate, pubsub }) => {
    const candidate = await Candidate.findOne({ _id: candidateId });

    if (!candidate) {
      throw new Error('Candidate does not exists.');
    }

    return await candidate.updateOnlineStatus(isOnline, pubsub);
  },
  updateOnlineStatusWithToken: async (parent, { isOnline, candidateId }, { Candidate, activeCandidate, pubsub }) => {
    const candidate = await Candidate.findOne({ _id: activeCandidate.candidate._id });

    if (!candidate) {
      throw new Error('Candidate does not exists.');
    }

    return await candidate.updateOnlineStatus(isOnline, pubsub);
  },
  updateCandidate: async (parent, { candidate }, { Candidate, activeCandidate, pubsub }) => {
    const updatedCandidate = await Candidate.findByIdAndUpdate(activeCandidate.candidate._id, candidate);

    if (!updatedCandidate) {
      throw new Error('Candidate does not exists.');
    }

    return updatedCandidate;
  },
  updateCandidateById: async (parent, { candidate, candidateId }, { Candidate, activeCandidate, pubsub }) => {
    console.log(candidate, candidateId);
    const updatedCandidate = await Candidate.findByIdAndUpdate(candidateId, candidate);

    if (!updatedCandidate) {
      throw new Error('Candidate does not exists.');
    }

    return updatedCandidate;
  },
  sendAdditionalQuestion: async (parent, { candidateId, title, name }, { Candidate, pubsub }) => {
    const candidate = await Candidate.findOne({ _id: candidateId });

    if (!candidate) {
      throw new Error('Candidate does not exists.');
    }

    const updatedCandidate = await candidate.sendAdditionalQuestion(title, name, pubsub);

    if (!updatedCandidate) {
      throw new Error('Candidate does not exists.');
    }

    return updatedCandidate;
  },
  updateGoogleTokensByTalentEmail: async (parent, { talentEmail, tokens }, { Candidate, pubsub }) => {
    const candidate = await Candidate.findOne({ talentEmail });

    if (!candidate) {
      throw new Error('Candidate does not exists.');
    }

    if (!tokens) {
      throw new Error('Tokens must be send.');
    }

    const updatedCandidate = await candidate.updateTokens(tokens);

    if (!updatedCandidate) {
      throw new Error('Candidate does not updated.');
    }

    return updatedCandidate;
  },
};
