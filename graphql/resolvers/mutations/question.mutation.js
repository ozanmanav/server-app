module.exports = {
  createQuestion: async (parent, { title, category, optionType, options, type, name }, { Question, pubsub }) => {
    const question = await new Question({
      title,
      category,
      optionType,
      type,
      options,
      name,
    }).save();

    return question;
  },
};
