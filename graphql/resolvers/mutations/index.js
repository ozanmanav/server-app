const candidate = require('./candidate.mutation');
const recruiter = require('./recruiter.mutation');
const question = require('./question.mutation');
const resumeUpload = require('./resumeUpload.mutation');
const resumeLink = require('./resumeLink.mutation');
const answer = require('./answer.mutation');
const position = require('./position.mutation');
const call = require('./call.mutation');
const jobCard = require('./jobCard.mutation');
const opportunity = require('./opportunity.mutation');
const scheduledCall = require('./scheduledCall.mutation');

const Mutation = {
  ...candidate,
  ...recruiter,
  ...question,
  ...resumeUpload,
  ...resumeLink,
  ...answer,
  ...position,
  ...call,
  ...jobCard,
  ...opportunity,
  ...scheduledCall,
};

module.exports = Mutation;
