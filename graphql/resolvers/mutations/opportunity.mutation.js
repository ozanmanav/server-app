const { draftMailHandler } = require('../../../helpers/gmailHandlers');
module.exports = {
  addOpportunity: async (parent, { jobCardId, candidateId, status }, { Opportunity, JobCard, pubsub }) => {
    const jobCard = await JobCard.findById(jobCardId).populate({
      path: 'recruiter',
      populate: [{ path: 'recruiters' }],
    });

    if (!jobCard) {
      throw new Error('JobCard does not exists.');
    }

    const newOpportunity = await new Opportunity({
      candidate: candidateId,
      recruiter: jobCard.recruiter._id,
      jobCard: jobCardId,
      status,
    })
      .save()
      .then(newOpportunity => newOpportunity.populate('jobCard').execPopulate())
      .then(newOpportunity => newOpportunity.populate('candidate').execPopulate());

    const opportunitiesCount = await Opportunity.find({ candidate: candidateId, status: 'pending' });

    if (newOpportunity && pubsub) {
      newOpportunity.opportunitiesCount = opportunitiesCount.length;
      pubsub.publish('OPPORTUNITY_ADDED', {
        opportunityListener: newOpportunity,
      });
    }

    return newOpportunity;
  },
  updateOpportunity: async (parent, args, { Opportunity, pubsub, SmartReply }) => {
    const isNotInterested = args.status === 'notInterested';

    const updatedOpportunity = await Opportunity.findByIdAndUpdate(args.opportunityId, {
      $set: args,
    })
      .populate('candidate')
      .populate('recruiter');

    const { recruiter, candidate } = updatedOpportunity;

    const pendingSmartReply = await SmartReply.findOne({ opportunityId: args.opportunityId, isSent: false });

    if (pendingSmartReply) {
      const { candidateTalentEmail, threadId, to } = pendingSmartReply.smartReplyContent;
      let { body, attachmentURL } = pendingSmartReply.smartReplyContent;

      let recruiterName = recruiter.name
        .split(' ')
        .slice(0, -1)
        .join(' ');

      if (isNotInterested) {
        // Override body
        body = `Hi ${recruiterName},<br/><br/> Thanks for your email but I am not interested in the position you sent.<br/><br/>${
          candidate.name
        }`;

        attachmentURL = false;
      }

      const resultDraftEmail = await draftMailHandler(candidateTalentEmail, body, threadId, attachmentURL, to);

      if (!resultDraftEmail) {
        throw new Error('Interested Draft Email send problem. ', resultDraftEmail);
      }

      if (!isNotInterested) {
        pendingSmartReply.isSent = true;
        pendingSmartReply.sentAt = new Date();
        await pendingSmartReply.save();
      }
    } else {
      console.log(args);
      console.log('Smart Reply Not Found');
    }

    return updatedOpportunity;
  },
};
