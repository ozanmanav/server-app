module.exports = {
  createPosition: async (parent, { title }, { Position }) => {
    const position = await new Position({
      title,
    }).save();

    return position;
  },
  addQuestionToPosition: async (parent, { title, questionId }, { Position, Question }) => {
    const question = await Question.findOne({ _id: questionId }).exec();

    if (!question) {
      throw new Error('Question not found.');
    }

    const position = await Position.findOneAndUpdate({ title }, { $addToSet: { questions: question._id } })
      .populate('questions')
      .exec();

    return position;
  },
};
