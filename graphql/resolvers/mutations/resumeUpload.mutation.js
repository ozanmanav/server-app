const shortid = require('shortid');
const mkdirp = require('mkdirp');
const moment = require('moment');
const axios = require('axios');
const querystring = require('querystring');
const path = require('path');
const { uploadFileToS3, deleteObjectInS3 } = require('@helpers/s3');
const fs = require('fs');

const UPLOAD_DIR = './uploads';

// Ensure upload directory exists.
mkdirp.sync(UPLOAD_DIR);

const storeFS = ({ stream, filename }) => {
  const id = shortid.generate();
  const path = `${UPLOAD_DIR}/${id}-${filename}`;
  return new Promise((resolve, reject) =>
    stream
      .on('error', error => {
        if (stream.truncated)
          // Delete the truncated file.
          fs.unlinkSync(path);
        reject(error);
      })
      .pipe(fs.createWriteStream(path))
      .on('error', error => reject(error))
      .on('finish', () => resolve({ id, path }))
  );
};

module.exports = {
  singleUpload: async (parent, { file }, { Candidate, ResumeUpload, pubsub, activeCandidate }) => {
    const candidate = await Candidate.findOne({ _id: activeCandidate.candidate._id });

    if (!candidate) {
      throw new Error('Candidate does not exists.');
    }

    if (!file) {
      throw new Error('Resume file is required');
    }

    const resume = await ResumeUpload.findOne({ candidate: activeCandidate.candidate._id });

    if (resume) {
      throw new Error('Resume already exists');
    }

    const { createReadStream, filename, mimetype, encoding } = await file;

    const stream = await createReadStream();

    // Save file to FS for testing
    // const { id, path } = await storeFS({ stream, filename });
    const onlyName = path.parse(filename).name;
    const extension = path.parse(filename).ext;

    const formattedFileName = await `${onlyName}_${moment(new Date()).format('DDMMYYYYHHSS')}${extension}`;

    const { Location, ETag, Bucket, Key } = await uploadFileToS3(
      stream,
      { name: formattedFileName },
      process.env.AWS_S3_RESUME_BUCKET
    );

    if (!Location) {
      throw new Error('Resume is not uploaded.');
    }

    const uploadedResume = await new ResumeUpload({
      filename: formattedFileName,
      mimetype,
      path: Location,
      url: process.env.AWS_S3_URL_PREFIX + Key,
      candidate,
      isActive: true,
      ETag,
      Bucket,
      Key,
    }).save();

    return uploadedResume;
  },
  deleteResumeUpload: async (parent, { filename }, { Candidate, ResumeUpload, activeCandidate }) => {
    const candidate = await Candidate.findOne({ _id: activeCandidate.candidate._id });

    if (!candidate) {
      throw new Error('Candidate does not exists.');
    }

    if (!filename) {
      throw new Error('Resume filename is required');
    }

    const resume = await ResumeUpload.findOne({ candidate: activeCandidate.candidate._id, filename });

    if (!resume) {
      throw new Error('Resume not found');
    }

    return deleteObjectInS3(resume.Key, process.env.AWS_S3_RESUME_BUCKET).then(
      value => {
        //Delete from collections.
        resume.remove();

        return {
          wasDeleted: true,
        };
      },
      error => {
        throw new Error(error);
      }
    );
  },
};
