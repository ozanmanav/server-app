const { google } = require('googleapis');
const { Candidate } = require('../models');
const axios = require('axios');
const querystring = require('querystring');
const sleep = waitTimeInMs => new Promise(resolve => setTimeout(resolve, waitTimeInMs));
module.exports = {
  refreshAccessTokensJob: async parameter => {
    try {
      // Pull all candidates
      const candidates = await Candidate.find({});
      let refreshedUsers = 0;
      for (const candidate of candidates) {
        if (candidate.refreshToken) {
          console.log(candidate.email);
          const accessTokenObj = await axios.post(
            'https://www.googleapis.com/oauth2/v4/token',
            querystring.stringify({
              refresh_token: candidate.refreshToken,
              client_id: process.env.CLIENT_ID,
              client_secret: process.env.CLIENT_SECRET,
              grant_type: 'refresh_token',
            })
          );
          candidate.accessToken = accessTokenObj.data.access_token;
          candidate.updatedAt = new Date();
          candidate.save();
          refreshedUsers++;
        }
      }

      console.log('[SCHEDULED JOB] refreshAccessTokensJob: refreshed ~>', refreshedUsers);
    } catch (error) {
      console.error(error);
    }
  },
  watchCandidateEmailsJob: async parameter => {
    try {
      // Pull all candidates
      const candidates = await Candidate.find({});

      for (const candidate of candidates) {
        if (candidate.refreshToken && candidate.accessToken) {
          var oAuth2Client = null;
          var gmail = null;
          oAuth2Client = new google.auth.OAuth2(
            process.env.CLIENT_ID,
            process.env.CLIENT_SECRET,
            process.env.CALLBACK_URL
          );

          oAuth2Client.setCredentials({
            access_token: candidate.accessToken,
            token_type: 'Bearer',
            refresh_token: candidate.refreshToken,
            id_token: candidate.idToken,
          });

          var gmail = await google.gmail({
            version: 'v1',
            auth: oAuth2Client,
          });

          await gmail.users.watch(
            {
              userId: 'me',
              resource: {
                topicName: 'projects/talentenvoy-mobile-mvp/topics/talentEnvoyTopic',
                labelIds: ['INBOX', 'SENT'],
              },
            },
            (err, data) => {
              if (err) {
                // console.info(`error in watchEmail${err}`);
                console.info(err.response.data.error.errors);
                // return reject(err);
              } else {
                console.info(
                  `[ScheduledJob] Watching Email for candidate id ~ ${candidate.id} - ${candidate.talentEmail}`
                );
              }
            }
          );

          await sleep(3000);
        }
      }
    } catch (error) {
      console.error(error);
    }
  },
};

require('make-runnable');
