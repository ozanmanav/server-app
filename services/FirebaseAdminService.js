var admin = require('firebase-admin');

module.exports = {
  init: async newCandidate => {
    try {
      var serviceAccount = require('../firebaseServiceAccount.json');
      admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
      });
    } catch (e) {
      throw new Error(e);
    }
  },
  sendNotificationWithMessage: async (fcmToken, title, body) => {
    try {
      if (fcmToken && title && body) {
        const message = {
          notification: {
            title,
            body,
          },
          token: fcmToken,
        };
        admin
          .messaging()
          .send(message)
          .then(response => {
            // Response is a message ID string.
            console.log('Successfully sent message:', response);
            return true;
          })
          .catch(error => {
            console.log('Error sending message:', error);
            return false;
          });
      }
    } catch (error) {
      console.error(error);
    }
  },
  sendNotificationByStatus: async newCandidate => {
    try {
      let message;
      switch (newCandidate.status) {
        case '1_1':
          message = {
            notification: {
              title: 'Update resume',
              body: `We reviewed your resume. We've detected an error in your resume, please upload it again.`,
            },
            token: newCandidate.fcmToken,
          };
          break;

        case '1_2':
          message = {
            notification: {
              title: 'Answer Questions',
              body: `We've reviewed your resume, but we have a few more questions. Please answer the following questions.`,
            },
            token: newCandidate.fcmToken,
          };
          break;
        case '1_3':
          message = {
            notification: {
              title: 'Unfortunately…',
              body: `We have reviewed your profile and unfortunately, we decided not to work together.`,
            },
            token: newCandidate.fcmToken,
          };
          break;
        case '1_4':
          message = {
            notification: {
              title: 'Unfortunately…',
              body: `We have reviewed your profile and unfortunately, we decided not to work together.`,
            },
            token: newCandidate.fcmToken,
          };
          break;
        case '2':
          message = {
            notification: {
              title: 'Congrats, ' + newCandidate.name,
              body: `We have reviewed and approved the profile. Let's start!`,
            },
            token: newCandidate.fcmToken,
          };
          break;

        case '4':
          message = {
            notification: {
              title: 'You’re on the Batch!',
              body: `We started to offer you to recruiters on 19.11.2018. You will receive calls intensively throughout the 2 week.`,
            },
            token: newCandidate.fcmToken,
          };
          break;

        default:
          break;
      }

      if (message) {
        admin
          .messaging()
          .send(message)
          .then(response => {
            // Response is a message ID string.
            console.log('Successfully sent message:', response);
            return true;
          })
          .catch(error => {
            console.log('Error sending message:', error);
            return false;
          });
      }
    } catch (e) {
      throw new Error(e);
    }
  },
};
