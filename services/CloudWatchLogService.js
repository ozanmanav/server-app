var winston = require('winston');
var WinstonCloudWatch = require('winston-cloudwatch');
const AWS = require('aws-sdk');

var NODE_ENV = process.env.NODE_ENV || 'development';

const logger = new winston.createLogger({
  transports: [
    // new winston.transports.Console({
    //   timestamp: true,
    //   colorize: true,
    // }),
    new WinstonCloudWatch({
      awsAccessKeyId: process.env.AWS_ACCESS_KEY_ID,
      awsSecretKey: process.env.AWS_SECRET_ACCESS_KEY,
      awsRegion: 'us-west-2',
      logGroupName: 'server-app-staging-logs',
      logStreamName: NODE_ENV,
    }),
  ],
});

module.exports = logger;
