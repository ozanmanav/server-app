module.exports = {
  WAITING_APPROVE: '1',
  WAITING_UPDATE_RESUME: '1_1',
  WAITING_ADDITIONAL_QUESTIONS: '1_2',
  UNFORTUNATELY: '1_3',
  UNFORTUNATELY2: '1_4',
  WAITING_QA: '2',
};
