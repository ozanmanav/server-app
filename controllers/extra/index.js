const { JobCard, Recruiter, Candidate, Opportunity, SmartReply } = require('../../models');
const { draftMailHandler } = require('../../helpers/gmailHandlers');
const { sendNotificationWithMessage } = require('../../services/FirebaseAdminService');
const bcrypt = require('bcrypt');
const axios = require('axios');
const { apolloPubSub } = require('../../app');

exports.addOpportunity = async (req, res) => {
  try {
    const { recruiter, jobCard, candidateTalentEmail, jdResumeMatchRatio } = req.body;

    if (!recruiter) {
      throw new Error('recruiter required.');
    }

    if (!jobCard) {
      throw new Error('jobCard required.');
    }

    if (!candidateTalentEmail) {
      throw new Error('candidateTalentEmail required.');
    }

    let foundRecruiter = await Recruiter.findOne({ email: recruiter.email });

    // 1- Create Recruiter if does not exist.
    if (!foundRecruiter) {
      foundRecruiter = await new Recruiter({
        name: recruiter.name,
        email: recruiter.email,
        recruiterNumbers: recruiter.recruiterNumbers,
        password: bcrypt.hashSync(recruiter.password, 10),
        provider: 'external',
      }).save();

      if (!foundRecruiter) {
        throw new Error('Cannot create new recruiter');
      }
    }

    const candidate = await Candidate.findOne({ talentEmail: candidateTalentEmail });

    if (!candidate) {
      throw new Error('Candidate does not exists.');
    }

    // 2- Create JobCard with this recruiter.
    const { companyName, location, title, contractType, positionType, description, refNo } = jobCard;

    const newJobCard = await new JobCard({
      recruiter: foundRecruiter.id,
      companyName,
      location,
      title,
      positionType,
      contractType,
      description,
      refNo,
      createdAt: new Date(),
    }).save();

    if (!newJobCard) {
      throw new Error('Couldnt create new job card');
    }

    // 3- Create Opportunity with this recruiter and assign to candidate.
    const newOpportunity = await new Opportunity({
      candidate: candidate.id,
      recruiter: foundRecruiter.id,
      jobCard: newJobCard.id,
      jdResumeMatchRatio,
      status: 'pending',
      createdAt: new Date(),
    })
      .save()
      .then(newOpportunity => newOpportunity.populate('jobCard').execPopulate());

    if (!newOpportunity) {
      throw new Error('Couldnt create new opportunity');
    }

    if (newOpportunity && apolloPubSub) {
      console.log('published');
      apolloPubSub.publish('OPPORTUNITY_ADDED', {
        opportunityListener: newOpportunity,
      });
    }

    // 4- Send notification to Candidate.
    await sendNotificationWithMessage(
      candidate.fcmToken,
      'New Opportunity',
      `You have new opportunity from ${companyName}`
    );
    if (res) {
      res.send({ success: true, opportunity: newOpportunity });
    } else {
      res.send({ success: false });
    }
  } catch (error) {
    console.log(error);
    res.send({ success: false, error: error.stack });
  }
};

exports.addDraft = async (req, res) => {
  try {
    const { candidateTalentEmail, body, threadId, attachmentURL, to } = req.body;
    if (!candidateTalentEmail) {
      res.status(500).json('candidateTalentEmail is required');
    }

    if (!body) {
      res.status(500).json('body is required');
    }

    if (!threadId) {
      res.status(500).json('threadId is required');
    }

    if (!to) {
      res.status(500).json('to is required');
    }

    const result = await draftMailHandler(candidateTalentEmail, body, threadId, attachmentURL, to);
    res.status(200).json(result);
  } catch (e) {
    res.status(500).json(e);
  }
};

exports.addSmartReply = async (req, res) => {
  try {
    const { opportunityId, smartReplyContent, isSent, sentAt } = req.body;

    let foundSmartReply = await SmartReply.findOne({ opportunityId });

    if (!foundSmartReply) {
      foundSmartReply = await new SmartReply({
        createdAt: new Date(),
        opportunityId,
        smartReplyContent,
        isSent,
        sentAt,
      }).save();
      res.send(foundSmartReply);
    }

    foundSmartReply.smartReplyContent = smartReplyContent;
    foundSmartReply.isSent = isSent;
    foundSmartReply.sentAt = sentAt;
    await foundSmartReply.save();

    res.send(foundSmartReply);
  } catch (e) {
    console.log(e);
    res.status(500).json(e);
  }
};

exports.triggerResumeParser = async (req, res) => {
  try {
    console.log('#### triggerResumeParser ###');
    const { Key, candidate_email } = req.body;

    const candidate = await Candidate.findOne({ email: candidate_email });

    if (!candidate) {
      throw new Error('Candidate does not exists.');
    }

    const parserResult = await axios
      .post(process.env.NLP_RESUME_PARSER_ENDPOINT, {
        resume_path: process.env.AWS_S3_RESUME_BUCKET + '/' + Key,
        candidate_email: candidate.talentEmail,
        timestamp: new Date(),
      })
      .then(function(response) {
        console.log(response);
        res.send(response);
      })
      .catch(function(error) {
        res.send(error);
      });
  } catch (e) {
    console.log(e);
    res.status(500).json(e);
  }
};
