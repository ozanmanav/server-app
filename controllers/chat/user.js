const IntercomSDK = require('../../helpers/IntercomSDK');

exports.userList = async (req, res) => {
  try {
    const result = await IntercomSDK.users.list();
    res.status(200).json(result.body);
  } catch (e) {
    res.status(500).json(e);
  }
};

exports.getUser = async (req, res) => {
  try {
    const { userId } = req.body;
    const result = await IntercomSDK.users.find({ id: userId });
    res.status(200).json(result.body);
  } catch (e) {
    res.status(500).json(e);
  }
};
