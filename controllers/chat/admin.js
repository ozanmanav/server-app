const IntercomSDK = require('../../helpers/IntercomSDK');

exports.get = async (req, res) => {
  try {
    const { adminId } = req.body;
    const result = await IntercomSDK.admins.find(adminId);
    res.status(200).json(result.body);
  } catch (e) {
    res.status(500).json(e);
  }
};
