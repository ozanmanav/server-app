const IntercomSDK = require('../../helpers/IntercomSDK');

exports.messageList = async (req, res) => {
  try {
    const { userId } = req.body;
    const result = await IntercomSDK.conversations.list({
      type: 'user',
      intercom_user_id: userId,
    });
    res.status(200).json(result.body);
  } catch (e) {
    res.status(500).json(e);
  }
};

exports.message = async (req, res) => {
  try {
    const { messageId } = req.body;
    const result = await IntercomSDK.conversations.find({ id: messageId });
    res.status(200).json(result.body);
  } catch (e) {
    res.status(500).json(e);
  }
};

exports.create = async (req, res) => {
  try {
    const { body, from_id, to_id } = req.body;
    const message = {
      message_type: 'inapp',
      subject: 'Hey',
      body: body,
      template: 'plain',
      from: {
        type: 'admin',
        id: from_id,
      },
      to: {
        type: 'user',
        id: to_id,
      },
    };
    const result = await IntercomSDK.messages.create(message);
    res.status(200).json(result.body);
  } catch (e) {
    res.status(500).json(e);
  }
};

exports.replyForUser = async (req, res) => {
  try {
    const { conversationId, body, userId } = req.body;
    const reply = {
      id: conversationId,
      intercom_user_id: userId,
      body,
      type: 'user',
      message_type: 'comment',
    };
    const result = await IntercomSDK.conversations.reply(reply);

    res.status(200).json(result.body);
  } catch (e) {
    res.status(500).json(e);
  }
};

exports.replyForAdmin = async (req, res) => {
  try {
    const { conversationId, body, adminId } = req.body;
    const reply = {
      id: conversationId,
      admin_id: adminId,
      body,
      type: 'admin',
      message_type: 'comment',
    };
    console.log(reply);
    const result = await IntercomSDK.conversations.reply(reply);

    res.status(200).json(result.body);
  } catch (e) {
    res.status(500).json(e);
  }
};
