const { Candidate, GmailHistory, CandidateEmails, ResumeUpload } = require('../models');
const { google } = require('googleapis');
const { getMailFromMessageId, getHistory, createDraft } = require('./gmailOperations');
var { Base64 } = require('js-base64');
const fetch = require('fetch-base64');
const MailComposer = require('nodemailer/lib/mail-composer');

const watchingMailHandler = async (googlePubSub, message) => {
  try {
    console.log(`Received message ${message.id}:`);
    console.log(`${message.data}`);

    const messageData = JSON.parse(message.data);

    const candidate = await Candidate.findOne({ talentEmail: messageData.emailAddress });

    if (!candidate) {
      return; //console.log('Candidate not found');
    }

    const oAuth2Client = new google.auth.OAuth2(
      process.env.CLIENT_ID,
      process.env.CLIENT_SECRET,
      process.env.CALLBACK_URL
    );

    oAuth2Client.setCredentials({
      access_token: candidate.accessToken,
      token_type: 'Bearer',
      refresh_token: candidate.refreshToken,
      id_token: candidate.idToken,
    });

    var gmail = google.gmail({
      version: 'v1',
      auth: oAuth2Client,
    });

    // We need find prev History Id
    const targetGmailHistory = await GmailHistory.findOne({ candidateId: candidate._id });

    if (!targetGmailHistory) {
      const newGmailHistory = await new GmailHistory({
        candidateId: candidate.id,
        prevHistory: null,
        currHistory: messageData.historyId,
      }).save();
      console.log('new history created');
    } else {
      const updatedGmailHistory = await GmailHistory.findByIdAndUpdate(targetGmailHistory._id, {
        $set: {
          prevHistory: targetGmailHistory.currHistory,
          currHistory: messageData.historyId,
        },
      });
      console.log('history record updated');
    }

    const historyList = await getHistory(
      gmail,
      targetGmailHistory && (targetGmailHistory.prevHistory || targetGmailHistory.currHistory),
      'INBOX'
    );
    if (historyList && historyList.history) {
      const messageIds = historyList.history.reduce((acc, m) => {
        m.messagesAdded.forEach(({ message }) => {
          acc.push({ id: message.id });
        });
        return acc;
      }, []);

      const lastMessageItem = messageIds.pop();

      const lastMail = await getMailFromMessageId(gmail, lastMessageItem.id);

      const searchedLastMail = await CandidateEmails.findOne({ emailId: lastMail.id });

      if (searchedLastMail) {
        return console.log('Mail already saved.');
      }

      if (lastMail.labelIds.includes('INBOX')) {
        const newCandidateEmail = await new CandidateEmails({
          candidateId: candidate._id,
          candidateEmail: candidate.email,
          talentEmail: candidate.talentEmail,
          emailPayload: lastMail,
          emailId: lastMessageItem.id,
          customLabelId: 'INBOX',
          createdAt: new Date(),
        }).save();

        console.log('this is inbox mail publish to channel');
        const lastMailObjectB64 = Buffer.from(JSON.stringify(newCandidateEmail));

        googlePubSub.topic('projects/talentenvoy-mobile-mvp/topics/newEmailTopic').publish(lastMailObjectB64);
      } else if (lastMail.labelIds.includes('SENT')) {
        const newCandidateEmail = await new CandidateEmails({
          candidateId: candidate._id,
          candidateEmail: candidate.email,
          talentEmail: candidate.talentEmail,
          emailPayload: lastMail,
          emailId: lastMessageItem.id,
          customLabelId: 'SENT',
          createdAt: new Date(),
        }).save();
        console.log('this is sent mail only saved to db');
      }
    } else {
      console.log('history list not found');
    }

    message.ack();
  } catch (error) {
    console.log('Parse error', error);
  }
};

const draftMailHandler = async (candidateTalentEmail, body, threadId, attachmentURL, to) => {
  try {
    const candidate = await Candidate.findOne({ talentEmail: candidateTalentEmail });

    if (!candidate) {
      throw new Error('Candidate Doesnt exist.');
    }

    const oAuth2Client = new google.auth.OAuth2(
      process.env.CLIENT_ID,
      process.env.CLIENT_SECRET,
      process.env.CALLBACK_URL
    );

    oAuth2Client.setCredentials({
      access_token: candidate.accessToken,
      token_type: 'Bearer',
      refresh_token: candidate.refreshToken,
      id_token: candidate.idToken,
    });

    var gmail = google.gmail({
      version: 'v1',
      auth: oAuth2Client,
    });

    const thread = await gmail.users.threads.get({
      userId: 'me',
      id: threadId,
    });

    const headersOfLastMessage = thread.data.messages[thread.data.messages.length - 1].payload.headers;

    let inReplyTo = '';
    let references = '';
    if (thread.data.messages.length < 2) {
      // First message is threadid equals messageid pass Message-Id to inreply and references
      var messageIdHeader = headersOfLastMessage.filter(
        obj => obj.name === 'Message-ID' || obj.name === 'Message-Id'
      )[0].value;
      inReplyTo = messageIdHeader;
      references = [messageIdHeader];
    } else {
      // Multi message included in thread use last message in reply to and references
      inReplyTo = headersOfLastMessage.filter(obj => obj.name === 'In-Reply-To')[0].value;
      references = [headersOfLastMessage.filter(obj => obj.name === 'References')[0].value];
    }
    let attachmentBase64;
    if (attachmentURL) {
      const resumeUpload = await ResumeUpload.findOne({ candidate });
      console.log(resumeUpload);
      attachmentBase64 = await fetch.remote(resumeUpload.url);
      console.log(attachmentBase64);
      var filename = resumeUpload.url.substring(resumeUpload.url.lastIndexOf('/') + 1);
    }

    var mail = new MailComposer({
      from: candidateTalentEmail,
      to,
      attachments: attachmentURL ? [{ filename, path: 'data:application/pdf;base64,' + attachmentBase64[0] }] : [],
      text: body,
      html: body,
      inReplyTo,
      references,
    });

    const message = await mail.compile();
    const built = await message.build();

    const draftMailBase64 = await Base64.encodeURI(built.toString('utf8'));

    const draftResult = await createDraft(gmail, draftMailBase64, threadId);
    return draftResult;
  } catch (error) {
    console.log(error);
    return error;
  }
};

module.exports = { watchingMailHandler, draftMailHandler };
