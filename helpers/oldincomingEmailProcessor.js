const _async = require('async');
const { google } = require('googleapis');
const schedule = require('node-schedule');

const Candidate = require('../models/Candidate');
const gmailHistory = require('../models/GmailHistory');
const gmailLabels = require('../models/GmailLabels');
const candidateEmails = require('../models/CandidateEmails');
const redis = require('./redis');
const { getHistory, getMailFromMessageId, modifyLabels, addToContact, postMailContents } = require('./oldGmailUtil');
const { REDIS_KEY } = process.env;

const dotenv = require('dotenv');
dotenv.config();

schedule.scheduleJob('0 0 * * *', async function(fireDate) {
  await redis.del(REDIS_KEY);
});

module.exports = async function(request, response) {
  try {
    console.info('***************************** Received new Email *****************************');
    console.info(request.body);

    // Disable pubsub processing temporarily
    // return response.status(200).send({});

    // 1. Get data from body
    const { message } = request.body;
    const b64string = message.data;
    const data = Buffer.from(b64string, 'base64').toString();

    console.info('*****************************incoming***********************************');
    console.info('DATA', data);
    // 2. destructure emailAddress and historyId from parsed data
    const { emailAddress, historyId } = JSON.parse(data);

    const targetCandidate = await Candidate.findOne({ email: emailAddress });

    // 3. Find target Candidate
    if (targetCandidate.email) {
      const { accessToken, refreshToken } = targetCandidate;

      if (!accessToken || !refreshToken) {
        return response.send({});
      }

      // 4. Find history record
      const targetHistoryRecord = await gmailHistory.findOne({ candidateId: targetCandidate._id });

      let targetHistoryId, matchingInboxHistory, matchingSentHistory;

      const promisesToResolve = [];
      let shouldResolvePromises = false;
      if (targetHistoryRecord && targetHistoryRecord.candidateId) {
        if (!targetHistoryRecord.currHistory) {
          return response.send({});
        }

        const updateGamilHistory = await gmailHistory.findByIdAndUpdate(targetHistoryRecord._id, {
          $set: {
            prevHistory: targetHistoryRecord.currHistory,
            currHistory: historyId,
          },
        });

        targetHistoryId = targetHistoryRecord.prevHistory || targetHistoryRecord.currHistory;
      } else {
        await new gmailHistory({
          candidateId: targetCandidate.id,
          prevHistory: null,
          currHistory: historyId,
        }).save();
        return response.send({});
      }

      // 5. Setup google OAuth2 client
      const client_id = process.env.CLIENT_ID;
      const client_secret = process.env.CLIENT_SECRET;
      const redirect_uri = process.env.CALLBACK_URL;
      const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uri);

      oAuth2Client.setCredentials({
        access_token: accessToken,
        token_type: 'Bearer',
        refresh_token: refreshToken,
      });

      // 6. Init gmail client
      let gmail = google.gmail({ version: 'v1', auth: oAuth2Client });

      // 7. Get the history data (mails) for inbox and sent.
      try {
        const [inboxHistory, sentHistory] = await Promise.all([
          getHistory(gmail, targetHistoryId, 'INBOX'),
          getHistory(gmail, targetHistoryId, 'SENT'),
        ]);

        if (!inboxHistory) {
          throw new Error('no inboxHistory');
        } else if (!sentHistory) {
          throw new Error('no sentHistory');
        } else {
          matchingInboxHistory = inboxHistory.history || [];
          matchingSentHistory = sentHistory.history || [];
        }
      } catch (e) {
        const { credentials } = await oAuth2Client.refreshAccessToken();
        oAuth2Client.setCredentials({
          access_token: credentials.access_token,
          token_type: 'Bearer',
          refresh_token: refreshToken,
        });

        await Candidate.findByIdAndUpdate(targetCandidate._id, {
          $set: {
            accessToken: credentials.access_token,
          },
        });

        gmail = google.gmail({ version: 'v1', auth: oAuth2Client });
        const [inboxHistory, sentHistory] = await Promise.all([
          getHistory(gmail, targetHistoryId, 'INBOX'),
          getHistory(gmail, targetHistoryId, 'SENT'),
        ]);
        matchingInboxHistory = inboxHistory.history || [];
        matchingSentHistory = sentHistory.history || [];
      }

      console.info({ matchingInboxHistory: JSON.stringify(matchingInboxHistory) });
      console.info({ matchingSentHistory: JSON.stringify(matchingSentHistory) });

      if (matchingInboxHistory.length === 0 && matchingSentHistory.length === 0) {
        return response.send({});
      }

      // 8. Process Inbox and Sent mails.
      const myMainLabel = await gmailLabels.find({ candidateId: targetCandidate.id, type: 'main' });

      if (myMainLabel.length) {
        const processedIds = JSON.parse(await redis.get(REDIS_KEY)) || [];

        let inboxMessageIds = [];
        let sentMessageIds = [];

        const task = () => {
          inboxMessageIds = matchingInboxHistory
            .reduce((acc, m) => {
              m.messagesAdded.forEach(({ message }) => {
                if (message.labelIds.length === 2 || message.labelIds.length === 3 || message.labelIds.length === 4) {
                  if (message.labelIds.includes('INBOX') && message.labelIds.includes('UNREAD')) {
                    if (message.labelIds.includes('IMPORTANT') || message.labelIds.includes('CATEGORY_PERSONAL')) {
                      acc.push({ id: message.id, type: 'inbox' });
                    } else if (message.labelIds.length === 2) {
                      acc.push({ id: message.id, type: 'inbox' });
                    }
                  }
                }
              });
              return acc;
            }, [])
            .filter(({ id }) => !processedIds.includes(id));

          sentMessageIds = matchingSentHistory
            .reduce((acc, m) => {
              m.messagesAdded.forEach(({ message }) => {
                if (message.labelIds.includes('SENT')) {
                  acc.push({ id: message.id, type: 'sent' });
                }
              });
              return acc;
            }, [])
            .filter(({ id }) => !processedIds.includes(id));

          processedIds.push(...inboxMessageIds.map(({ id }) => id));
          processedIds.push(...sentMessageIds.map(({ id }) => id));

          const messageIds = inboxMessageIds.concat(sentMessageIds);

          if (messageIds.length) shouldResolvePromises = true;
          console.info('messageIds', messageIds);

          return new Promise((resolve, reject) => {
            _async.each(
              messageIds,
              ({ type: thizMessageType, id: thizMessageId }, callback) => {
                getMailFromMessageId(gmail, thizMessageId).then(message_payload => {
                  return new candidateEmails({
                    candidateId: targetCandidate.id,
                    EmailPayload: message_payload,
                    EmailId: thizMessageId,
                  }).save((error, data) => {
                    if (error) {
                      throw new Error('Saving email error');
                    }
                    postMailContents(targetCandidate.id, message_payload).then(parserResponse => {
                      return callback();
                    });
                  });
                });
              },
              err => {
                if (err) {
                  return reject(err);
                } else {
                  return resolve(true);
                }
              }
            );
          });
        };

        await task();

        await redis.set(REDIS_KEY, JSON.stringify(processedIds));
        if (promisesToResolve.length && shouldResolvePromises) {
          await Promise.all(promisesToResolve);
        }
      }
    }
    return response.send({});
  } catch (error) {
    console.info('catch block', error);
    return response.status(500).send({});
  }
};
