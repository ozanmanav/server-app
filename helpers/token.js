const jwt = require('jsonwebtoken');

const token = {
  generate: (candidate, expiresIn = '7d') => {
    return jwt.sign({ candidate }, process.env.SECRET_KEY, { expiresIn });
  },
  generateRecruiter: (recruiter, expiresIn = '7d') => {
    return jwt.sign({ recruiter }, process.env.SECRET_KEY, { expiresIn });
  },
};

module.exports = token;
