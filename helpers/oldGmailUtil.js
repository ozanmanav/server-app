const querystring = require('querystring');
const axios = require('axios');
const _async = require('async');
const GoogleContacts = require('google-contacts-crud');

const gmailLabels = require('../models/GmailLabels');
const candidateContactMapping = require('../models/CandidateContactMapping');
const { PubSub } = require('@google-cloud/pubsub');

const pubsub = new PubSub({
  projectId: 'parsing-api',
  keyFilename: process.env.GOOGLE_APPLICATION_CREDENTIALS,
});
const topicName = process.env.PUBSUB_PARSING_API_TOPIC_NAME;

async function refreshToken() {
  try {
    const accessTokenObj = await axios.post(
      'https://www.googleapis.com/oauth2/v4/token',
      querystring.stringify({
        refresh_token: refreshToken,
        client_id: process.env.CLIENT_ID,
        client_secret: process.env.CLIENT_SECRET,
        grant_type: 'refresh_token',
      })
    );
    return accessTokenObj.data.access_token;
  } catch (err) {
    console.info(err);
  }
}

function getHistory(gmail, startHistoryId, labelId) {
  return new Promise((resolve, reject) => {
    gmail.users.history.list(
      {
        userId: 'me',
        startHistoryId,
        historyTypes: 'messageAdded',
        labelId,
      },
      (err, data) => {
        if (err) {
          console.info(`error in getHistory${err}`);
          return reject(err);
        } else {
          console.info(`history for ${labelId} ~ ${JSON.stringify(data.data)}`);
          return resolve(data.data);
        }
      }
    );
  });
}

function getMailFromMessageId(gmail, messageId) {
  return new Promise((resolve, reject) => {
    gmail.users.messages.get(
      {
        userId: 'me',
        id: messageId,
      },
      (err, data) => {
        if (err) {
          return reject(err);
        } else {
          return resolve(data.data);
        }
      }
    );
  });
}

function createLabel(gmail, labelName) {
  return new Promise((resolve, reject) => {
    gmail.candidates.labels.create(
      {
        candidateId: 'me',
        resource: {
          name: labelName,
          labelListVisibility: 'labelShow',
          messageListVisibility: 'show',
        },
      },
      (err, { data }) => {
        if (err) {
          console.info('CreateLabel Error');
          return reject(err);
        } else {
          console.info('createLabel Success');
          console.info(data);
          return resolve(data);
        }
      }
    );
  });
}

async function modifyLabels(
  gmail,
  messageId,
  labelIdsToAdd,
  removeLabelIds = [],
  labelsToCreateAndAdd,
  targetCandidate,
  parserResponse
) {
  try {
    let addLabelIds = [];
    if (labelsToCreateAndAdd.length) {
      const { id } = targetCandidate;
      const candidateGmailLabels = await gmailLabels.find({ candidateId: id });
      const candidateGmailLabelsNames = candidateGmailLabels.map(({ name }) => name);
      addLabelIds = parserResponse.predicted_labels
        .filter(predicted_label => candidateGmailLabelsNames.includes(predicted_label))
        .map(predicted_label => candidateGmailLabels.find(ugl => ugl.name === predicted_label).googleLabelId);
      const labelsToCreate = parserResponse.predicted_labels.filter(
        predicted_label => !candidateGmailLabelsNames.includes(predicted_label)
      );
      console.info('Labels to create');
      console.info(labelsToCreate);
      const task = () => {
        const arr = [];
        return new Promise((resolve, reject) => {
          _async.each(
            labelsToCreate,
            (thizLabelName, cb) => {
              createLabel(gmail, thizLabelName)
                .then(createdLabel => {
                  arr.push({
                    id: createdLabel.id,
                    name: createdLabel.name,
                    messageListVisibility: createdLabel.messageListVisibility,
                    labelListVisibility: createdLabel.labelListVisibility,
                    type: 'custom',
                  });
                  cb();
                })
                .catch(() => {
                  cb();
                });
            },
            err => {
              if (err) {
                reject(err);
              } else {
                return resolve(arr);
              }
            }
          );
        });
      };
      // createdLabelIds.push(await task());
      const processedLabelIds = await task();
      console.info('processedLabelIds');
      console.info(processedLabelIds);
      processedLabelIds.forEach(processedId => {
        addLabelIds.push(processedId.id);
      });
      processedLabelIds.forEach(async processedId => {
        const { name, messageListVisibility, labelListVisibility, type } = processedId;

        await new gmailLabels({
          candidateId: id,
          googleLabelId: processedId.id,
          name,
          messageListVisibility,
          labelListVisibility,
          type,
        }).save();
      });
    }
    console.info('gmail label payload');
    console.info({
      addLabelIds,
      candidateId: 'me',
      id: messageId,
      resource: {
        addLabelIds: labelIdsToAdd.concat(addLabelIds),
        removeLabelIds,
      },
    });
    await gmail.candidates.messages.modify({
      candidateId: 'me',
      id: messageId,
      resource: {
        addLabelIds: labelIdsToAdd.concat(addLabelIds),
        removeLabelIds,
      },
    });
    return true;
  } catch (error) {
    console.info('error in modify labels');
    console.info(error);
    throw error;
  }
}

async function addToContact(name, email, phone, targetCandidate, recruiter_id, shouldUpdateContact) {
  console.info('add To contact args');
  console.info({ name, email, phone, targetCandidate, recruiter_id, shouldUpdateContact });
  try {
    const { googleAccessToken, refreshToken } = targetCandidate;
    const googleContacts = new GoogleContacts(process.env.CLIENT_ID, process.env.CLIENT_SECRET);
    const credential = {
      access_token: googleAccessToken,
      expiry_date: 3600,
      refresh_token: refreshToken,
      token_type: 'Bearer',
    };
    googleContacts.setCandidateCredentials(credential);
    const createContactTask = () => {
      return new Promise((resolve, reject) => {
        const createData = {
          name,
          display_name: name,
          email,
          is_primary: true,
          contact_type: 'other',
          phoneNumber: phone ? phone : 'other',
          headers: {
            'GData-Version': '3.0',
            'Candidate-Agent': 'SomeAgent',
          },
        };
        googleContacts.addContact(createData, function(error, contact) {
          if (error) {
            console.info('addToContact Error');
            console.info(error);
            return reject(error);
          }
          console.info('Added contact');
          console.info(contact);
          return resolve(contact);
        });
      });
    };
    const updateContactTask = googleContactId => {
      return new Promise((resolve, reject) => {
        const updateData = {
          headers: {
            'GData-Version': '3.0',
            'Candidate-Agent': 'SomeAgent',
          },
          ...(name ? { name } : {}),
          ...(email ? { email } : {}),
          ...(phone ? { phoneNumber: phone } : {}),
        };
        console.info('updateContactTask updateData');
        console.info(updateData);
        googleContacts.updateContacts(
          {
            contact_id: googleContactId,
          },
          updateData,
          function(error, data) {
            console.info('updateContactTask Response');
            console.info(data);
            console.info('updateContactTask Error');
            console.info(error);
            return resolve(data);
          }
        );
      });
    };
    if (shouldUpdateContact === false) {
      const googleContact = await createContactTask();
      if (recruiter_id) {
        const mappingRecord = await candidateContactMapping.find({
          recruiter_id: recruiter_id,
          candidate_id: targetCandidate.id,
        });

        if (mappingRecord.length === 0) {
          await new candidateContactMapping({
            recruiter_id,
            candidate_id: targetCandidate.id,
            google_contact_id: googleContact.contacts[0].id,
          }).save();
        }
      }
    } else {
      // update in google
      if (recruiter_id) {
        const mappingRecord = await candidateContactMapping.find({
          recruiter_id: recruiter_id,
          candidate_id: targetCandidate.id,
        });
        if (mappingRecord.length) {
          const { google_contact_id } = mappingRecord[0];
          await updateContactTask(google_contact_id);
        }
      }
    }
    return true;
  } catch (err) {
    console.info('addToContact Error');
    console.info(err);
    throw err;
  }
}

async function postMailContents(candidate_id, message_payload) {
  try {
    let environment = 'test';
    if (process.env.NODE_ENV) {
      if (process.env.NODE_ENV === 'production') {
        environment = 'production';
      }
    }

    const data = JSON.stringify({
      candidate_id,
      message_payload: message_payload,
      environment,
    });

    const dataBuffer = Buffer.from(data);

    const messageId = await pubsub
      .topic(topicName)
      .publisher()
      .publish(dataBuffer);

    return messageId;
  } catch (error) {
    console.info('Error in posting mail contents');
    console.info({ message: 'Error in posting mail contents', candidate_id, message_payload });
    throw error;
  }
}

module.exports = {
  getHistory,
  getMailFromMessageId,
  createLabel,
  modifyLabels,
  addToContact,
  postMailContents,
};
