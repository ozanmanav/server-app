function watchEmail(gmail, newCandidate) {
  return new Promise((resolve, reject) => {
    gmail.users.watch(
      {
        userId: 'me',
        resource: {
          topicName: 'projects/talentenvoy-mobile-mvp/topics/talentEnvoyTopic',
          labelIds: ['INBOX', 'SENT'],
        },
      },
      (err, data) => {
        if (err) {
          console.info(`error in watchEmail${err}`);
          return reject(err);
        } else {
          console.info(`Watching Email for candidate id ~ ${newCandidate.id}`);
          return resolve(data.data);
        }
      }
    );
  });
}

function getHistory(gmail, startHistoryId, labelId) {
  return new Promise((resolve, reject) => {
    gmail.users.history.list(
      {
        userId: 'me',
        startHistoryId,
        historyTypes: 'messageAdded',
        labelIds: ['INBOX', 'SENT'],
      },
      (err, data) => {
        if (err) {
          console.info(`error in getHistory${err}`);
          return reject(err);
        } else {
          console.info(`history for ${labelId} ~ ${JSON.stringify(data.data)}`);
          return resolve(data.data);
        }
      }
    );
  });
}

function getMailFromMessageId(gmail, messageId) {
  return new Promise((resolve, reject) => {
    gmail.users.messages.get(
      {
        userId: 'me',
        id: messageId,
      },
      (err, data) => {
        if (err) {
          console.info(`error in getMailFromMessageId ${err}`);
          return reject(err);
        } else {
          console.info(`getting mail from message id ${messageId}`);
          return resolve(data.data);
        }
      }
    );
  });
}

function createDraft(gmail, draftMailBase64, threadId) {
  return new Promise((resolve, reject) => {
    gmail.users.drafts.create(
      {
        userId: 'me',
        resource: {
          message: {
            threadId,
            raw: draftMailBase64,
          },
        },
      },
      (err, data) => {
        if (err) {
          console.info(`error in createDraft ${err}`);
          return reject(err);
        } else {
          console.info(`created Draft`);
          return resolve(data.data);
        }
      }
    );
  });
}

module.exports = { watchEmail, getHistory, getMailFromMessageId, createDraft };
