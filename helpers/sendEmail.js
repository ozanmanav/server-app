const nodemailer = require('nodemailer');
const emailConfig = {
  service: 'gmail',
  email: 'noreply@talentenvoy.com',
  password: process.env.NOREPLY_PASSWORD,
  emailFrom: '"Talent Envoy" <noreply@talentenvoy.com>',
};

function sendEmail(candidate, redirectLink) {
  try {
    var transporter = nodemailer.createTransport({
      service: emailConfig.service,
      auth: {
        user: emailConfig.email,
        pass: emailConfig.password,
      },
    });

    // setup email data with unicode symbols
    const mailOptions = {
      from: emailConfig.emailFrom,
      to: candidate.email,
      subject: 'Reset your password',
      html: `<div>
          <p>Hi ${candidate.name},</p>
          <p>
              You have requested a password reset, please follow the link below to reset your password.
          </p>
          <p>
              Please ignore this email if you did not request a password change.
          </p>
  
          <p>
            <a href="${redirectLink}">
              Follow this link to reset your password.
            </a>
          </p>
          <br />
          <p>Thanks,</p>
          <p>Talent Envoy Team</p>
          </div>`,
    };

    // send mail with defined transport object
    return new Promise(function(resolve, reject) {
      transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          reject(error);
        }
        resolve(info);
      });
    });
  } catch (error) {
    console.log(error);
  }
}

module.exports = sendEmail;
