const AWS = require('aws-sdk');
const { readFileSync } = require('fs');
const { extname } = require('path');

const S3 = new AWS.S3({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  signatureVersion: 'v4',
});

const uploadFileToS3 = (stream, options = {}, BUCKET) => {
  const fileName = options.name || String(Date.now());

  return new Promise((resolve, reject) => {
    const params = {
      Bucket: BUCKET,
      ACL: 'public-read',
      Key: `${fileName}`,
      Body: stream,
    };
    return S3.upload(params, function(err, data) {
      if (err) {
        console.log('Error: ', err);
        reject(err);
      } else {
        console.log('Data', data);
        resolve(data);
      }
    });
  });
};

const deleteObjectInS3 = (Key, BUCKET) => {
  return new Promise((resolve, reject) => {
    const params = {
      Bucket: BUCKET,
      Key,
    };
    S3.deleteObject(params, function(err, data) {
      if (err) return reject(err);
      else return resolve(true);
    });
  });
};

module.exports = {
  S3,
  uploadFileToS3,
  deleteObjectInS3,
};
