const express = require('express');
const userRouter = require('./chat/user');
const messageRouter = require('./chat/message');
const adminRouter = require('./chat/admin');
const extraRouter = require('./extra');

const router = express.Router();

router.use('/chat/user', userRouter);
router.use('/chat/message', messageRouter);
router.use('/chat/admin', adminRouter);

router.use('/extra', extraRouter);

module.exports = router;
