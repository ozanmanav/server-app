const express = require('express');

const adminController = require('../../controllers/chat/admin');

const router = express.Router();

router.post('/', adminController.get);

module.exports = router;
