const express = require('express');

const messageController = require('../../controllers/chat/message');

const router = express.Router();

router.post('/list', messageController.messageList);
router.post('/', messageController.message);
router.post('/reply-user', messageController.replyForUser);
router.post('/reply-admin', messageController.replyForAdmin);
router.post('/initiate', messageController.create);

module.exports = router;
