const express = require('express');

const userController = require('../../controllers/chat/user');

const router = express.Router();

router.get('/list', userController.userList);
router.post('/', userController.getUser);

module.exports = router;
