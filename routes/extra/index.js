const express = require('express');

const extraController = require('../../controllers/extra');

const router = express.Router();

// Create Recruiter -> Create Job Card with Recruiter -> Create Opportunity and assign candidate with jobcard.
router.post('/addOpportunity', extraController.addOpportunity);
router.post('/addDraft', extraController.addDraft);
router.post('/addSmartReply', extraController.addSmartReply);
router.post('/triggerResumeParser', extraController.triggerResumeParser);

module.exports = router;
