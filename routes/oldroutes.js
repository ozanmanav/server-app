const app = (module.exports = require('express')());
const bodyParser = require('body-parser');
const { JobCard, Recruiter, Candidate } = require('../models');
const processorIncomingEmail = require('../helpers/incomingEmailProcessor');
const path = require('path');

app.get('/', (req, res) => {
  const memoryUsed = process.memoryUsage().heapUsed / 1024 / 1024;
  res.send({ msg: 'hello! Server is up and running', memoryUsed });
});

app.get('/googlee14c90dc0aedf471.html', function(req, res) {
  res.sendFile(path.join(__dirname + '/googlee14c90dc0aedf471.html'));
});

// POST ENDPOINTS
app.post('/new_message_webhook', bodyParser.json(), processorIncomingEmail);

app.post('/addParsedEmail', bodyParser.json(), async function(req, res) {
  try {
    const {
      candidate_id,
      messageId,
      emails_in_body,
      job_skills,
      location,
      job_titles,
      organization,
      people,
      phone,
      recruiter_id,
      sender_domain,
      sender_email,
      sender_name,
      snippet,
      subject,
      timestamp,
      plain_body,
      predicted_label,
      jd_match_ratio,
    } = req.body.parsed_email;

    const response = await new ParsedEmail({
      candidate_id,
      messageId,
      emails_in_body,
      job_skills,
      location,
      job_titles,
      organization,
      people,
      phone,
      recruiter_id,
      sender_domain,
      sender_email,
      sender_name,
      snippet,
      subject,
      timestamp,
      plain_body,
      predicted_label,
      jd_match_ratio,
    }).save();

    if (res) {
      res.send({ success: true });
    } else {
      res.send({ success: false });
    }
  } catch (error) {
    res.send({
      success: false,
      error: error,
    });
  }
});

app.post('/addJobCard', bodyParser.json(), async function(req, res) {
  try {
    const { candidate_email, recruiter_email, parsed_jd } = req.body;

    if (!parsed_jd) {
      throw new Error('Parsed JD required.');
    }

    const { job_description, job_titles, location, organization } = parsed_jd;

    const recruiter = await Recruiter.findOne({ email: recruiter_email });

    if (!recruiter) {
      throw new Error('Recruiter does not exists.');
    }

    const candidate = await Candidate.findOne({ email: candidate_email });

    if (!candidate) {
      throw new Error('Candidate does not exists.');
    }

    // First Create Job Card with this parsed_jd
    const newJobCard = await new JobCard({
      recruiter: recruiter.id,
      companyName: organization,
      location,
      title: job_titles[0],
      contractType,
      description: job_description,
      refNo,
    }).save();

    // Second Create Opportunity with Candidate
    const newOpportunity = await new Opportunity({
      candidate: candidate.id,
      recruiter: newJobCard.recruiter._id,
      jobCard: newJobCard.id,
      status,
    })
      .save()
      .then(newOpportunity => newOpportunity.populate('jobCard').execPopulate())
      .then(newOpportunity => newOpportunity.populate('candidate').execPopulate());

    if (res) {
      res.send({ success: true });
    } else {
      res.send({ success: false });
    }
  } catch (error) {
    res.send({
      success: false,
      error: error.message,
    });
  }
});
